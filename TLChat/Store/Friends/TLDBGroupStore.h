//
//  TLDBGroupStore.h
//  LES,GO
//
//  Created by liuchao on 16/6/24.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLDBBaseStore.h"

@interface TLDBGroupStore : TLDBBaseStore

- (BOOL)updateGroupsData:(NSArray *)groupData
                   forUid:(NSString *)uid;

- (BOOL)addGroup:(TLGroup *)group forUid:(NSString *)uid;


- (NSMutableArray *)groupsDataByUid:(NSString *)uid;

- (BOOL)deleteGroupByGid:(NSString *)gid forUid:(NSString *)uid;

@end
