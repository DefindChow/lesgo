//
//  TLDBFriendStore.h
//  LES,GO
//
//  Created by liuchao on 16/6/24.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLDBBaseStore.h"

@interface TLDBFriendStore : TLDBBaseStore

- (BOOL)updateFriendsData:(NSArray *)friendData
                   forUid:(NSString *)uid;

- (BOOL)addFriend:(TLUser *)user forUid:(NSString *)uid;

- (NSMutableArray *)friendsDataByUid:(NSString *)uid;

- (BOOL)deleteFriendByFid:(NSString *)fid forUid:(NSString *)uid;

@end
