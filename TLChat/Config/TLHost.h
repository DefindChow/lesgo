//
//  TLHost.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLHost : NSObject

+ (NSString *)clientInitInfoURL;

#pragma mark - Mine

+ (NSString *)expressionURLWithEid:(NSString *)eid;

+ (NSString *)expressionDownloadURLWithEid:(NSString *)eid;

@end
