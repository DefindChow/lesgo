//
//  TLEventStatistics.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#ifndef TLEventStatistics_h
#define TLEventStatistics_h

#define     EVENT_GET_CONTACTS          @"e_get_contacts"
#define     EVENT_DELETE_MESSAGE        @"e_delete_message"

#endif /* TLStatistics_h */
