//
//  TLAboutHeaderView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLAboutHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *imagePath;

@end
