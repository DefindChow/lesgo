//
//  TLPrivacySettingViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLPrivacySettingViewController.h"
#import "TLPrivacySettingHelper.h"

@interface TLPrivacySettingViewController ()

@property (nonatomic, strong) TLPrivacySettingHelper *helper;

@end

@implementation TLPrivacySettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"隐私"];
    
    self.helper = [[TLPrivacySettingHelper alloc] init];
    self.data = self.helper.minePrivacySettingData;
}

@end
