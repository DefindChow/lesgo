//
//  TLChatFontSettingView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

#define     MIN_FONT_SIZE           15.0f
#define     STANDARD_FONT_SZIE      16.0f
#define     MAX_FONT_SZIE           20.0f

@interface TLChatFontSettingView : UIView

@property (nonatomic, assign) CGFloat curFontSize;

@property (nonatomic, copy) void (^fontSizeChangeTo)(CGFloat size);


@end
