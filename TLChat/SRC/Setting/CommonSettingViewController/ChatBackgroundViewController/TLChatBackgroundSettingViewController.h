//
//  TLChatBackgroundSettingViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLSettingViewController.h"

@interface TLChatBackgroundSettingViewController : TLSettingViewController

/**
 *  若为nil则全局设置，否则只给对应好友设置
 */
@property (nonatomic, assign) NSString *partnerID;

@end
