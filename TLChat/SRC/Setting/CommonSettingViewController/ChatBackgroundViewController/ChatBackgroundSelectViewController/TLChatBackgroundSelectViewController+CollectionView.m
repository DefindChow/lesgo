//
//  TLChatBackgroundSelectViewController+CollectionView.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLChatBackgroundSelectViewController+CollectionView.h"
#import "TLChatBackgroundSelectCell.h"

@implementation TLChatBackgroundSelectViewController (CollectionView)

- (void)registerCellForCollectionView:(UICollectionView *)collectionView
{
    [collectionView registerClass:[TLChatBackgroundSelectCell class] forCellWithReuseIdentifier:@"TLChatBackgroundSelectCell"];
}

#pragma mark - # Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.data.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TLChatBackgroundSelectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TLChatBackgroundSelectCell" forIndexPath:indexPath];
    return cell;
}

@end
