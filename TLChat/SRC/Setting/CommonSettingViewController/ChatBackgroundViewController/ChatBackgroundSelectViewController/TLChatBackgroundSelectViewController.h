//
//  TLChatBackgroundSelectViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLChatBackgroundSelectViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic, strong) UICollectionView *collectionView;

@end
