//
//  TLNewMessageSettingViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLNewMessageSettingViewController.h"
#import "TLNewMessageSettingHelper.h"

@interface TLNewMessageSettingViewController ()

@property (nonatomic, strong) TLNewMessageSettingHelper *helper;

@end

@implementation TLNewMessageSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"新消息通知"];
    
    self.helper = [[TLNewMessageSettingHelper alloc] init];
    self.data = self.helper.mineNewMessageSettingData;
}

@end
