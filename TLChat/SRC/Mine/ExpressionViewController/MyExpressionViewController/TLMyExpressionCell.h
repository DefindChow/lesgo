//
//  TLMyExpressionCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TLEmojiGroup.h"

@protocol TLMyExpressionCellDelegate <NSObject>

- (void)myExpressionCellDeleteButtonDown:(TLEmojiGroup *)group;

@end

@interface TLMyExpressionCell : UITableViewCell

@property (nonatomic, assign) id<TLMyExpressionCellDelegate>delegate;

@property (nonatomic, strong) TLEmojiGroup *group;

@end
