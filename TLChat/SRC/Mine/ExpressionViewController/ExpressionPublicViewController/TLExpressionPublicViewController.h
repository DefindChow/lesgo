//
//  TLExpressionPublicViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLViewController.h"
#import "TLExpressionProxy.h"

@interface TLExpressionPublicViewController : TLViewController
{
    NSInteger kPageIndex;
}

@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic, strong) TLExpressionProxy *proxy;

@property (nonatomic, strong) UICollectionView *collectionView;

@end
