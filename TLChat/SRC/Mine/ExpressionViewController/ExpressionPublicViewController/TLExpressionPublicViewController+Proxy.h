//
//  TLExpressionPublicViewController+Proxy.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLExpressionPublicViewController.h"

@interface TLExpressionPublicViewController (Proxy)

- (void)loadDataWithLoadingView:(BOOL)showLoadingView;

- (void)loadMoreData;

@end
