//
//  TLExpressionChosenViewController+Proxy.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLExpressionChosenViewController.h"

@interface TLExpressionChosenViewController (Proxy)

- (void)loadDataWithLoadingView:(BOOL)showLoadingView;

- (void)loadMoreData;

@end
