//
//  TLExpressionBannerCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"

@protocol TLExpressionBannerCellDelegate <NSObject>

- (void)expressionBannerCellDidSelectBanner:(id)item;

@end

@interface TLExpressionBannerCell : TLTableViewCell

@property (nonatomic, assign) id<TLExpressionBannerCellDelegate>delegate;

@property (nonatomic, strong) NSArray *data;

@end
