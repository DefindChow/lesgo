//
//  TLExpressionCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLTableViewCell.h"
#import "TLEmojiGroup.h"

@protocol TLExpressionCellDelegate <NSObject>

- (void)expressionCellDownloadButtonDown:(TLEmojiGroup *)group;

@end

@interface TLExpressionCell : TLTableViewCell

@property (nonatomic, assign) id<TLExpressionCellDelegate> delegate;

@property (nonatomic, strong) TLEmojiGroup *group;

@end
