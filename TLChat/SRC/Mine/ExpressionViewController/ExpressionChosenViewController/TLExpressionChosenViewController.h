//
//  TLExpressionChosenViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLTableViewController.h"
#import "TLExpressionHelper.h"
#import "TLExpressionProxy.h"

@interface TLExpressionChosenViewController : TLTableViewController
{
    NSInteger kPageIndex;
}

@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic, strong) NSArray *bannerData;

@property (nonatomic, strong) TLExpressionProxy *proxy;


@end
