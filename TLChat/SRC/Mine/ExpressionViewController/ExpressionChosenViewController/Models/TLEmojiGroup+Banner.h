//
//  TLEmojiGroup+Banner.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLEmojiGroup.h"
#import "TLPictureCarouselProtocol.h"

@interface TLEmojiGroup (Banner) <TLPictureCarouselProtocol>

@end
