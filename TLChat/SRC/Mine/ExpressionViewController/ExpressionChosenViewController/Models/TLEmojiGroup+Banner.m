//
//  TLEmojiGroup+Banner.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLEmojiGroup+Banner.h"

@implementation TLEmojiGroup (Banner)

- (NSString *)pictureURL
{
    return self.bannerURL;
}

@end
