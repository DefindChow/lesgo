//
//  TLExpressionChosenViewController+TableView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLExpressionChosenViewController.h"
#import "TLExpressionBannerCell.h"
#import "TLExpressionCell.h"

#define         HEIGHT_BANNERCELL       140.0f
#define         HEGIHT_EXPCELL          80.0f

@interface TLExpressionChosenViewController (TableView) <TLExpressionCellDelegate, TLExpressionBannerCellDelegate>

- (void)registerCellsForTableView:(UITableView *)tableView;

@end
