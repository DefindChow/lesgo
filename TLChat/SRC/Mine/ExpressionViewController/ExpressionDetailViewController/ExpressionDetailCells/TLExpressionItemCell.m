//
//  TLExpressionItemCell.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLExpressionItemCell.h"
#import <UIImageView+WebCache.h>
#import <UIImage+GIF.h>
#import "UIImage+Color.h"

@interface TLExpressionItemCell ()

@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation TLExpressionItemCell

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.imageView];
        
        [self p_addMasonry];
    }
    return self;
}

- (void)setEmoji:(TLEmoji *)emoji
{
    _emoji = emoji;
    UIImage *image = [UIImage imageNamed:emoji.emojiPath];
    if (image) {
        [self.imageView setImage:image];
    }
    else {
        [self.imageView sd_setImageWithURL:TLURL(emoji.emojiURL)];
    }
}

#pragma mark - # Private Methods
- (void)p_addMasonry
{
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.contentView);
    }];
}

#pragma mark - # Getter
- (UIImageView *)imageView
{
    if (_imageView == nil) {
        _imageView = [[UIImageView alloc] init];
        [_imageView.layer setMasksToBounds:YES];
        [_imageView.layer setCornerRadius:3.0f];
    }
    return _imageView;
}

@end
