//
//  TLExpressionDetailCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TLEmojiGroup.h"

#define         HEIGHT_EXP_BANNER       (WIDTH_SCREEN * 0.45)

@protocol TLExpressionDetailCellDelegate <NSObject>

- (void)expressionDetailCellDownloadButtonDown:(TLEmojiGroup *)group;

@end

@interface TLExpressionDetailCell : UICollectionViewCell

@property (nonatomic, assign) id <TLExpressionDetailCellDelegate> delegate;

@property (nonatomic, strong) TLEmojiGroup *group;

+ (CGFloat)cellHeightForModel:(TLEmojiGroup *)group;

@end
