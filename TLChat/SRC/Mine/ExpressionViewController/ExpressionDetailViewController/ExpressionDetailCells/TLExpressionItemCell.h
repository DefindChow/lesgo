//
//  TLExpressionItemCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TLEmoji.h"

@interface TLExpressionItemCell : UICollectionViewCell

@property (nonatomic, strong) TLEmoji *emoji;

@end
