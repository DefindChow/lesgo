//
//  TLExpressionDetailViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLViewController.h"
#import "TLImageExpressionDisplayView.h"
#import "TLEmojiGroup.h"

@interface TLExpressionDetailViewController : TLViewController

@property (nonatomic, strong) TLEmojiGroup *group;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) TLImageExpressionDisplayView *emojiDisplayView;

@end
