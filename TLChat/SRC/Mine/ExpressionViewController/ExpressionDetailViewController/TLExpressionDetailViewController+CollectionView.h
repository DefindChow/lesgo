//
//  TLExpressionDetailViewController+CollectionView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "TLExpressionDetailViewController.h"
#import "TLExpressionDetailCell.h"

@interface TLExpressionDetailViewController (CollectionView) <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, TLExpressionDetailCellDelegate>

- (void)registerCellForCollectionView:(UICollectionView *)collectionView;

- (void)didLongPressScreen:(UILongPressGestureRecognizer *)sender;

- (void)didTap5TimesScreen:(UITapGestureRecognizer *)sender;


@end
