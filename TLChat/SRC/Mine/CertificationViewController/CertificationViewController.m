//
//  CertificationViewController.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/25.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "CertificationViewController.h"
#import "CertificationHelper.h"

#import "IDCardViewController.h"

@interface CertificationViewController ()

@property (nonatomic, strong) CertificationHelper *helper;

@end

@implementation CertificationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"我的认证"];
    
    self.helper = [[CertificationHelper alloc] init];
    self.data = self.helper.CertificationData;
}

#pragma mark - Delegate -
//MARK: UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TLSettingItem *item = [self.data[indexPath.section] objectAtIndex:indexPath.row];
    if ([item.title isEqualToString:@"身份证认证"]) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:NSStringFromClass([IDCardViewController class]) bundle:nil];
        // 加载箭头指向控制器
         IDCardViewController *IDCardVC = [storyboard instantiateInitialViewController];
        //IDCardViewController *IDCardVC = [[IDCardViewController alloc] init];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:IDCardVC animated:YES];
    }
    
}

@end
