//
//  TLMineHeaderCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "TLUser.h"

@interface TLMineHeaderCell : UITableViewCell

@property (nonatomic, strong) TLUser *user;

@end
