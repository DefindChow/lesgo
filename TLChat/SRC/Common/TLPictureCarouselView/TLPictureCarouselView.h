//
//  TLPictureCarouselView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLPictureCarouselProtocol.h"

#define         DEFAULT_TIMEINTERVAL        5.0f

@class TLPictureCarouselView;
@protocol TLPictureCarouselDelegate <NSObject>

- (void)pictureCarouselView:(TLPictureCarouselView *)pictureCarouselView
              didSelectItem:(id<TLPictureCarouselProtocol>)model;

@end

@interface TLPictureCarouselView : UIView

@property (nonatomic, strong) NSArray *data;

@property (nonatomic, assign) id<TLPictureCarouselDelegate> delegate;

@property (nonatomic, assign) NSTimeInterval timeInterval;

@end
