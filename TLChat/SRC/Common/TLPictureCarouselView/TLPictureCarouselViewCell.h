//
//  TLPictureCarouselViewCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLPictureCarouselProtocol.h"

@interface TLPictureCarouselViewCell : UICollectionViewCell

@property (nonatomic, strong) id<TLPictureCarouselProtocol> model;

@end
