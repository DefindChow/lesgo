//
//  TLImageBrowserController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLImageBrowserController : UIViewController

@property (nonatomic, strong) NSArray *images;

@property (nonatomic, assign) NSInteger curIndex;

- (id)initWithImages:(NSArray *)images curImageIndex:(NSInteger)index curImageRect:(CGRect)rect;

- (void)show;

@end
