//
//  TLShoppingViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLShoppingViewController.h"

@implementation TLShoppingViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_shopping_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonDown:)];
    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
//    [self setUrl:@"http://wq.jd.com/"];
    [self setUrl:@"http://m.taobao.com"];
}

#pragma mark - Event Response
- (void) rightBarButtonDown:(UIBarButtonItem *)sender
{

}

@end
