//
//  TLDiscoverViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLDiscoverViewController.h"
#import "TLDiscoverHelper.h"

#import "TLMomentsViewController.h"//朋友圈
#import "TLScanningViewController.h"//扫一扫
#import "PackageViewController.h"//跟团游
#import "DIYViewController.h"//自由行
#import "NearbyViewController.h"//附近
#import "CommunityViewController.h"//社区

@interface TLDiscoverViewController ()

@property (nonatomic, strong) TLMomentsViewController *momentsVC;

@property (nonatomic, strong) TLDiscoverHelper *discoverHelper;

@end

@implementation TLDiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"发现"];//self.navigationItem.title = @"发现";
    self.discoverHelper = [[TLDiscoverHelper alloc] init];
    self.data = self.discoverHelper.discoverMenuData;
}

#pragma mark - Delegate - 
//MARK: UITableViewDelegate
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TLMenuItem *item = [self.data[indexPath.section] objectAtIndex:indexPath.row];
    if ([item.title isEqualToString:@"朋友圈"]) {
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:self.momentsVC animated:YES];
        [self setHidesBottomBarWhenPushed:NO];
    }
    if ([item.title isEqualToString:@"跟团游"]) {
        PackageViewController *packageVC = [[PackageViewController alloc] init];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:packageVC animated:YES];
        [self setHidesBottomBarWhenPushed:NO];
    }
    else if ([item.title isEqualToString:@"自由行"]) {
        DIYViewController *diyVC = [[DIYViewController alloc] init];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:diyVC animated:YES];
        [self setHidesBottomBarWhenPushed:NO];
    }
    else if ([item.title isEqualToString:@"扫一扫"]) {
        TLScanningViewController *scannerVC = [[TLScanningViewController alloc] init];
            [self setHidesBottomBarWhenPushed:YES];
            [self.navigationController pushViewController:scannerVC animated:YES];
            [self setHidesBottomBarWhenPushed:NO];
    }
    else if ([item.title isEqualToString:@"附近"]) {
        NearbyViewController *nearbyVC = [[NearbyViewController alloc] init];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:nearbyVC animated:YES];
        [self setHidesBottomBarWhenPushed:NO];
    }
    else if ([item.title isEqualToString:@"社区"]) {
        CommunityViewController *communityVC = [[CommunityViewController alloc] init];
        [self setHidesBottomBarWhenPushed:YES];
        [self.navigationController pushViewController:communityVC animated:YES];
        [self setHidesBottomBarWhenPushed:NO];
    }


    
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
}

#pragma mark - # Getter 
- (TLMomentsViewController *)momentsVC
{
    if (_momentsVC == nil) {
        _momentsVC = [[TLMomentsViewController alloc] init];
    }
    return _momentsVC;
}

@end
