//
//  NearbyViewController.m
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//


#import "NearbyViewController.h"
#import "NearbyCell.h"
#import "TLMacros.h"

@interface NearbyViewController ()

@end

@implementation NearbyViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"附近"];
    
    [self nearMan];


}

-(void)nearMan
{
    UIView *nearMan = [[[NSBundle mainBundle] loadNibNamed:@"NearbyCell" owner:nil options:nil]firstObject];
    nearMan.frame = CGRectMake(0, 0, SIZE_SCREEN.width, 100);
    
    [self.view addSubview:nearMan];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
