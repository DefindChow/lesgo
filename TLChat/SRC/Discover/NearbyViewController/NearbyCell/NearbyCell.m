//
//  NearbyCell.m
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "NearbyCell.h"
#import "NearbyModel.h"

@interface NearbyCell()

@property (weak, nonatomic) IBOutlet UIImageView *Userimage;
@property (weak, nonatomic) IBOutlet UILabel *UserName;
@property (weak, nonatomic) IBOutlet UILabel *sex;
@property (weak, nonatomic) IBOutlet UILabel *gap;//距离
@property (weak, nonatomic) IBOutlet UILabel *autograph;//个签
@property (weak, nonatomic) IBOutlet UIButton *Nearbutton;


@end



@implementation NearbyCell




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.UserName.textColor = [UIColor blackColor];
    self.sex.textColor = [UIColor blackColor];
    self.gap.textColor = [UIColor grayColor];
    self.autograph.textColor = [UIColor grayColor];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
