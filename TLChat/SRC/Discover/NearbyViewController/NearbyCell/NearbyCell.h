//
//  NearbyCell.h
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NearbyModel;
@interface NearbyCell : UITableViewCell
@property (nonatomic, strong) NearbyModel *item;

@end
