//
//  CommunityModel.h
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "BaseModel.h"

@interface CommunityModel : BaseModel


@property (nonatomic, strong) NSString *topic;//话题
@property (nonatomic, strong) NSString *focuNum;//关注数
@property (nonatomic, strong) NSString *postsBum;//帖子数
@property (nonatomic, strong) NSString *describe;//描述





@end
