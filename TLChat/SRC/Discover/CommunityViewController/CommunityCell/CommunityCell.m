//
//  CommunityCell.m
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "CommunityCell.h"
#import "CommunityModel.h"


@interface CommunityCell()

@property (weak, nonatomic) IBOutlet UILabel *topic;//话题

@property (weak, nonatomic) IBOutlet UILabel *focuNum;//关注数

@property (weak, nonatomic) IBOutlet UILabel *postsBum;//帖子数

@property (weak, nonatomic) IBOutlet UILabel *describe;//描述

- (IBAction)focuButton:(UIButton *)sender;//关注


@end

@implementation CommunityCell

-(void)setFrame:(CGRect)frame
{
    frame.size.height -= 1;
    //赋值
    [super setFrame:frame];

}


//处理关注数字
-(void)focuNum
{
    NSString *numStr = [NSString stringWithFormat:@"关注%@",_item.focuNum];
    NSInteger num = _item.focuNum.integerValue;
    if (num > 10000) {
        CGFloat numF = num / 10000.0;
        numStr = [NSString stringWithFormat:@"关注@%.1fW",numF];
        numStr = [numStr stringByReplacingOccurrencesOfString:@".0" withString:@""];
    }

    _focuNum.text = numStr;

}

//处理帖子数字
-(void)postsBum
{
    NSString *numStr = [NSString stringWithFormat:@"帖子%@",_item.postsBum];
    NSInteger num = _item.postsBum.integerValue;
    if (num > 10000) {
        CGFloat numF = num / 10000.0;
        numStr = [NSString stringWithFormat:@"帖子@%.1fW",numF];
        numStr = [numStr stringByReplacingOccurrencesOfString:@".0" withString:@""];
    }
    
    _postsBum.text = numStr;
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    // 设置头像圆角,iOS9苹果修复
    //    _iconView.layer.cornerRadius = 30;
    //    _iconView.layer.masksToBounds = YES;
    
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)focuButton:(UIButton *)sender {
    
}
@end
