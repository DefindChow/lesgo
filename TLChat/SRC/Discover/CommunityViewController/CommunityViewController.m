//
//  CommunityViewController.m
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//



#import "CommunityViewController.h"
#import "CommunityCell.h"
#import "CommunityModel.h"
#import <AFNetworking/AFNetworking.h>
#import <MJExtension/MJExtension.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <MJRefresh.h>
#import "APPHeader.h"
#import "TLMacros.h"

#define IME_URL @"http://yelaisex.com/cms/install_package/html/2016/sqtop_0819/1.html"

static NSString * const CommunityCellIdentifier = @"CommunityCell";
static NSString * const ID = @"cell";

@interface CommunityViewController ()

@property (nonatomic, strong) UITableView * tableview;

@property (nonatomic, strong) NSArray *subTags;
@property (nonatomic, weak) AFHTTPSessionManager *mgr;

@end

@implementation CommunityViewController

// 接口文档: 请求url(基本url+请求参数) 请求方式
- (void)viewDidLoad {
    [super viewDidLoad];
    
// 展示标签数据 -> 请求数据(接口文档) -> 解析数据(写成Plist)(image_list,sub_number,theme_name) -> 设计模型 -> 字典转模型 -> 展示数据
    [self.navigationItem setTitle:@"社区"];
    //[self createUI ];
    [self AddScrollerView];
   
    [self loadData];
   
    // 处理cell分割线 1.自定义分割线 2.系统属性(iOS8才支持) 3.万能方式(重写cell的setFrame) 了解tableView底层实现了解 1.取消系统自带分割线 2.把tableView背景色设置为分割线的背景色 3.重写setFrame
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
 
    self.tableView.backgroundColor = [UIColor colorWithRed:220 green:220 blue:221 alpha:1];

    // 提示用户当前正在加载数据 SVPro
    [SVProgressHUD showWithStatus:@"正在加载ing......"];
    
    //[self setupRefresh];
}



// 界面即将消失调用
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    // 销毁指示器
    [SVProgressHUD dismiss];
    
    // 取消之前的请求
    [_mgr.tasks makeObjectsPerformSelector:@selector(cancel)];
    
}


#pragma mark - 请求数据
- (void)loadData
{
    // 1.创建请求会话管理者
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    
    _mgr = mgr;
    
    // 2.拼接参数
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters[@"a"] = @"tag_recommend";
    parameters[@"action"] = @"sub";
    parameters[@"c"] = @"topic";
    
    //发送请求
    
    [mgr GET:SHRQU_URL parameters:parameters progress:nil success:^(NSURLSessionDataTask * task, NSArray *  responseObject){
        
        [SVProgressHUD dismiss];
        _subTags = [CommunityModel mj_objectArrayWithKeyValuesArray:responseObject];
    
        // 刷新表格
        [self.tableView reloadData];
    } failure:^(NSURLSessionDataTask *  task, NSError *  error) {
        [SVProgressHUD dismiss];
    }];

}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.subTags.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // 自定义cell
    CommunityCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    
    // 获取模型
    CommunityModel *item = self.subTags[indexPath.row];
    
    cell.item = item;
    
    return cell;
}



-(void)AddScrollerView
{
    NSArray *imagePathArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:@""];
    
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 64, SIZE_SCREEN.width, 200)];//64
    
    scroll.tag = 100;
    scroll.delegate = self;
    scroll.contentSize = CGSizeMake(self.view.bounds.size.width * imagePathArray.count, 200);
    scroll.pagingEnabled = YES;
    scroll.showsHorizontalScrollIndicator = NO;
    
    NSInteger i = 0;
    for (NSString * imageNamePath in imagePathArray) {
        NSArray * imageNameArray = [[imageNamePath lastPathComponent]componentsSeparatedByString:@"."];
        
        NSData * data = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageNameArray[0] ofType: imageNameArray[1]]];
        UIImage *image = [UIImage imageWithData:data];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width * i, 0, self.view.bounds.size.width, 200)];
        imageView.userInteractionEnabled = YES;
        imageView.image = image;
        i++;
        [scroll addSubview:imageView];
    }
    
    [self.view addSubview:scroll];
}

#pragma mark - UI
-(void)createUI
{
    UIView *Community = [[[NSBundle mainBundle] loadNibNamed:@"CommunityCell" owner:nil options:nil]firstObject];
    Community.frame = CGRectMake(0, 0, SIZE_SCREEN.width, 300);
    
    [self.view addSubview:Community];
    

    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


#pragma mark - 组头视图

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
