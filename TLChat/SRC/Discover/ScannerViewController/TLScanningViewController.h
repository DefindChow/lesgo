//
//  TLScanningViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLViewController.h"

@interface TLScanningViewController : TLViewController

/**
 *  禁用底部工具栏（默认NO，若开启，将只支持扫码）
 */
@property (nonatomic, assign) BOOL disableFunctionBar;

@end
