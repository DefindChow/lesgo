//
//  TLMomentDetailBaseView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMomentDetail.h"
#import "TLMomentViewDelegate.h"

@interface TLMomentDetailBaseView : UIView

@property (nonatomic, assign) id<TLMomentDetailViewDelegate> delegate;

@property (nonatomic, strong) TLMomentDetail *detail;

@end
