//
//  TLMomentMultiImageView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMomentViewDelegate.h"

@interface TLMomentMultiImageView : UIView

@property (nonatomic, assign) id<TLMomentMultiImageViewDelegate> delegate;

@property (nonatomic, strong) NSArray *images;

@end
