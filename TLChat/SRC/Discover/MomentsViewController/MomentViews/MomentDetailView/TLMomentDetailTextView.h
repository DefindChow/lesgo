//
//  TLMomentDetailTextView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentDetailBaseView.h"

@interface TLMomentDetailTextView : TLMomentDetailBaseView

@property (nonatomic, strong) UILabel *titleLabel;

@end
