//
//  TLMomentExtensionView+TableView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentExtensionView.h"

@interface TLMomentExtensionView (TableView) <UITableViewDelegate, UITableViewDataSource>

- (void)registerCellForTableView:(UITableView *)tableView;

@end
