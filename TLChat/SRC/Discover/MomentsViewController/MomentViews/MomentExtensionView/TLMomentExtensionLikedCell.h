//
//  TLMomentExtensionLikedCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"

@interface TLMomentExtensionLikedCell : TLTableViewCell

@property (nonatomic, strong) NSArray *likedFriends;

@end
