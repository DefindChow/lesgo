//
//  TLMomentExtensionCommentCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLMomentComment.h"

@interface TLMomentExtensionCommentCell : TLTableViewCell

@property (nonatomic, strong) TLMomentComment *comment;

@end
