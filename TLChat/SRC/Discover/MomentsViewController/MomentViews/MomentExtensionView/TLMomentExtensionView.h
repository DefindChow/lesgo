//
//  TLMomentExtensionView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMomentExtension.h"

@interface TLMomentExtensionView : UIView

@property (nonatomic, strong) TLMomentExtension *extension;

@end
