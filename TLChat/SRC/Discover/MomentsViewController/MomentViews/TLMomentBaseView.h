//
//  TLMomentBaseView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMomentViewDelegate.h"
#import "TLMoment.h"

@interface TLMomentBaseView : UIView

@property (nonatomic, assign) id<TLMomentViewDelegate> delegate;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIView *detailContainerView;

@property (nonatomic, strong) UIView *extensionContainerView;

@property (nonatomic, strong) TLMoment *moment;

@end
