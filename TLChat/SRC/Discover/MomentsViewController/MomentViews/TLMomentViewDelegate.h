//
//  TLMomentViewDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TLMomentMultiImageViewDelegate <NSObject>

- (void)momentViewClickImage:(NSArray *)images atIndex:(NSInteger)index;

@end


@protocol TLMomentDetailViewDelegate <TLMomentMultiImageViewDelegate>

@end


@protocol TLMomentViewDelegate <TLMomentDetailViewDelegate>

@end
