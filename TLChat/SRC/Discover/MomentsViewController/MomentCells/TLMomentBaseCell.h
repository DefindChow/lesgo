//
//  TLMomentBaseCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLMoment.h"
#import "TLMomentViewDelegate.h"

@interface TLMomentBaseCell : TLTableViewCell

@property (nonatomic, assign) id<TLMomentViewDelegate> delegate;

@property (nonatomic, strong) TLMoment *moment;

@end
