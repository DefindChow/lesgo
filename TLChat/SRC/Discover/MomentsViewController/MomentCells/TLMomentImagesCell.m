//
//  TLMomentImagesCell.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentImagesCell.h"
#import "TLMomentImageView.h"

@interface TLMomentImagesCell ()

@property (nonatomic, strong) TLMomentImageView *momentView;

@end

@implementation TLMomentImagesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self.contentView addSubview:self.momentView];
        [self.momentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)setMoment:(TLMoment *)moment
{
    [super setMoment:moment];
    [self.momentView setMoment:moment];
}

- (void)setDelegate:(id<TLMomentViewDelegate>)delegate
{
    [super setDelegate:delegate];
    [self.momentView setDelegate:delegate];
}

#pragma mark - # Getter
- (TLMomentImageView *)momentView
{
    if (_momentView == nil) {
        _momentView = [[TLMomentImageView alloc] init];
    }
    return _momentView;
}

@end
