//
//  TLMomentsViewController+Proxy.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLMomentsViewController.h"

@interface TLMomentsViewController (Proxy)

- (void)loadData;

@end
