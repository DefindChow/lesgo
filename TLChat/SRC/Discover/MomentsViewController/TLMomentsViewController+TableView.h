//
//  TLMomentsViewController+TableView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentsViewController.h"
#import "TLMomentViewDelegate.h"

@interface TLMomentsViewController (TableView) <TLMomentViewDelegate>

- (void)registerCellForTableView:(UITableView *)tableView;

@end
