//
//  TLMomentsViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentsViewController.h"
#import "TLMomentsViewController+TableView.h"
#import "TLMomentsViewController+Proxy.h"

@interface TLMomentsViewController ()

@end

@implementation TLMomentsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"朋友圈"];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    [self.tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 60.0f)]];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_camera"] style:UIBarButtonItemStylePlain actionBlick:^{
        
    }];
    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
    [self registerCellForTableView:self.tableView];
    [self loadData];
}

#pragma mark - # Getter
- (TLMomentsProxy *)proxy
{
    if (_proxy == nil) {
        _proxy = [[TLMomentsProxy alloc] init];
    }
    return _proxy;
}

@end
