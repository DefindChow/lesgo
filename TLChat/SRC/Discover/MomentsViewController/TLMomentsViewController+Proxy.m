//
//  TLMomentsViewController+Proxy.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMomentsViewController+Proxy.h"

@implementation TLMomentsViewController (Proxy)

- (void)loadData
{
    self.data = [NSMutableArray arrayWithArray:self.proxy.testData];
    [self.tableView reloadData];
}

@end
