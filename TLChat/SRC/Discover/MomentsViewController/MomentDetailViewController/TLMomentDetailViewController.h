//
//  TLMomentDetailViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLViewController.h"
#import "TLMoment.h"

@interface TLMomentDetailViewController : TLViewController

@property (nonatomic, strong) TLMoment *moment;

@end
