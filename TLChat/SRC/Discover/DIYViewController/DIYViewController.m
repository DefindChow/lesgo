//
//  DIYViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "DIYViewController.h"
#import <AFNetworking/AFNetworking.h>
#import <MJExtension/MJExtension.h>
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "APPHeader.h"
#import "DIYModel.h"
#import "TLMacros.h"
#import "DIYCell.h"

static NSString * const ID = @"cell";

@interface DIYViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *diyItems;
@property (nonatomic, strong) NSArray *diyModels;
@property (nonatomic, weak) AFHTTPSessionManager *mgr;
@property (nonatomic, assign) NSInteger pageIndex;

@end

@implementation DIYViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self loadData];
    
    // 注册cell
    [self.tableView registerNib:[UINib nibWithNibName:@"DIYCell" bundle:nil] forCellReuseIdentifier:ID];
    
    [self.navigationItem setTitle:@"自由行"];
    
    //self.navigationBarBackgroundHidden = YES;//(透明)隐藏导航栏
    
    self.automaticallyAdjustsScrollViewInsets = YES;

    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableView.backgroundColor = [UIColor colorWithRed:220/ 256.0 green:220/ 256.0 blue:221/ 256.0 alpha:1];
    // 提示用户当前正在加载数据 SVPro
    [SVProgressHUD showWithStatus:@"玩命加载中..."];
    
    //[self initDIY];
    [self setupRefresh];
}

-(void)setupRefresh
{
    _pageIndex = 1;
    [self loadData];
}


- (void)loadData
{
    __weak DIYViewController *weekSelf = self;
    NSURL *url = [NSURL URLWithString:DIY_URL];
    //创建请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    //请求体body
    request.HTTPBody = [@"catid=74" dataUsingEncoding:NSUTF8StringEncoding];
    //发送请求
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * respons, NSData * data ,NSError *  connectionError) {
        [SVProgressHUD dismiss];
        if (connectionError == nil){
            NSDictionary *diyDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
           // NSLog(@"diyDic = %@", diyDic);
            if (_pageIndex == 1) {
                _diyItems = [DIYModel mj_objectArrayWithKeyValuesArray:diyDic[@"data"]];
               // NSLog(@"_diyItems = %@", _diyItems);
            }else
            {
                [weekSelf.diyItems addObjectsFromArray:[DIYModel mj_objectArrayWithKeyValuesArray:diyDic[@"data"]]];
            }
            //刷新表格
            [self.tableView reloadData];
        }else{
            [SVProgressHUD dismiss];
            NSLog(@"error = %@", connectionError);
        }
    }];
}


#pragma mark - 点击事件
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    
}

#pragma mark - tableView的代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.diyItems.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 自定义cell
    DIYCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    //获取模型
    DIYModel *model = self.diyItems[indexPath.row];
    
    cell.model = model;
    
    return cell;
}

//高
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 220;
}


#pragma mark - 懒加载

-(NSMutableArray *)diyItems
{
    if (!_diyItems) {
        _diyItems = [[NSMutableArray alloc]init];
    }
    return _diyItems;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}


@end
