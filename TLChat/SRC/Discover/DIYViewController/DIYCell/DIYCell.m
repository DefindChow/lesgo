//
//  DIYCell.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/4.
//  Copyright © 2016年 刘超. All rights reserved.
//

#import "DIYCell.h"
#import "DIYModel.h"
#import <UIImageView+WebCache.h>

@interface DIYCell ()



@end

@implementation DIYCell



-(void)setModel:(DIYModel *)model
{
    _model = model;
     // 设置内容
    _title.text = model.title;
    [_diyimage sd_setImageWithURL:[NSURL URLWithString:model.thumb]];



}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
