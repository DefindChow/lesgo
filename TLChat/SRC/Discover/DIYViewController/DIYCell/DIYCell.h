//
//  DIYCell.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/4.
//  Copyright © 2016年 刘超. All rights reserved.
//


#import <UIKit/UIKit.h>
@class DIYModel;
@interface DIYCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *diyimage;

@property (weak, nonatomic) IBOutlet UILabel *title;

@property (nonatomic, strong) DIYModel *model;

@end
