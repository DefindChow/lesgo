//
//  PackageViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "PackageViewController.h"
#import "CurrentCell.h"
#import "NextCell.h"
#import "PackageDetailView.h"
#import "APPHeader.h"
#import "PackageModel.h"
#import "TLMacros.h"


@interface PackageViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray * sectionTitle;//分段标题
@property (nonatomic, strong) UICollectionView *PackagecollectionView;
@property (nonatomic, strong) NSMutableArray *PackageArr;
@property (nonatomic, assign) NSInteger pageIndex;

@end

@implementation PackageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"跟团游"];
    
    _sectionTitle = @[@"本期路线", @"下期路线"];
    [self loadData];
    [self createPackageUI];
   [SVProgressHUD showWithStatus:@"玩命加载中..."];
}

#pragma mark - 请求数据
- (void)loadData
{
     __weak PackageViewController *weekSelf = self;
    NSURL *url = [NSURL URLWithString:Package_URL];
    //创建请求
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    //请求体body
    request.HTTPBody = [@"posids=1" dataUsingEncoding:NSUTF8StringEncoding];
    //发送请求
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse * respons, NSData * data ,NSError *  connectionError) {
        [SVProgressHUD dismiss];
        if (connectionError == nil){
            NSDictionary *diyDic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
             NSLog(@"diyDic = %@", diyDic);
             if (_pageIndex == 1) {
                 _PackageArr = [PackageModel mj_objectArrayWithKeyValuesArray:diyDic[@"data"]];
                 NSLog(@"_PackageArr = %@", _PackageArr);
                    [weekSelf.PackageArr addObjectsFromArray:[PackageModel mj_objectArrayWithKeyValuesArray:diyDic[@"data"]]];
             }
            [self.PackagecollectionView reloadData];
        }else{
            [SVProgressHUD dismiss];
            NSLog(@"error = %@", connectionError);
        }
    }];


}

#pragma mark - 创建UICollectionView
-(void)createPackageUI
{
//    UIView *Current = [[[NSBundle mainBundle] loadNibNamed:@"CurrentCell" owner:nil options:nil]firstObject];
//    Current.frame = CGRectMake(0, 0, SIZE_SCREEN.width, 220);
//    [self.view addSubview:Current];
    // 创建布局
    UICollectionViewFlowLayout * layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    CGRect frame = self.view.bounds;
    frame.origin.y = 0;
    frame.size.height -= 44 + 64;
    
    _PackagecollectionView = [[UICollectionView alloc] initWithFrame:frame collectionViewLayout:layout];
    
    _PackagecollectionView.delegate = self;
    _PackagecollectionView.dataSource  =self;
    _PackagecollectionView.backgroundColor= [UIColor grayColor];
    [self.view addSubview:_PackagecollectionView];
    
    
    [_PackagecollectionView registerClass:[CurrentCell class] forCellWithReuseIdentifier:@"Current"];
    [_PackagecollectionView registerClass:[NextCell class] forCellWithReuseIdentifier:@"Next"];
}



#pragma mark - UICollectionView的代理
//行
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == 1) {
        return [_PackageArr count];
    }
    return 1;

}

//高
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return CGSizeMake(WIDTH_SCREEN, 200);
    } else {
        return CGSizeMake(WIDTH_SCREEN * 0.45, 0.45 * WIDTH_SCREEN + 40);
    }
}

//行距
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
       //static NSString *cellID = @"CurrentCell";
    CurrentCell *Ccell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Current" forIndexPath:indexPath];
        PackageDetailView *PackageDetail = [[PackageDetailView alloc]init];
        
        
        [self.navigationController pushViewController:PackageDetail animated:YES];
        return Ccell;
    }else {
        NextCell *Ncell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Next" forIndexPath:indexPath];
       
        PackageDetailView *PackageDetail = [[PackageDetailView alloc]init];
        
        [self.navigationController pushViewController:PackageDetail animated:YES];
    return Ncell;
    }
}



- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    CGFloat width = (WIDTH_SCREEN - 2 ) / 2;
    if (section == 1) {
        return UIEdgeInsetsMake(0, width, 0, width);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}




@end
