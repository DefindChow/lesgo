//
//  PackageModel.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "BaseModel.h"

@interface PackageModel : BaseModel

@property (nonatomic, strong) NSString *cfd;//出发地
@property (nonatomic, strong) NSString *cfsj;
@property (nonatomic, strong) NSString *xcxx;
@property (nonatomic, strong) NSString *jihexx;
@property (nonatomic, strong) NSString *fyxz;
@property (nonatomic, strong) NSString *xlyh;
@property (nonatomic, strong) NSString *gjfw;
@property (nonatomic, strong) NSString *kf;
@property (nonatomic, strong) NSString *xcts;
@property (nonatomic, strong) NSString *jg;
@property (nonatomic, strong) NSString *bqlx;
@property (nonatomic, strong) NSString *tp;
@property (nonatomic, strong) NSString *xlts;

//@property (nonatomic, strong) PackageModel *model;






@end
