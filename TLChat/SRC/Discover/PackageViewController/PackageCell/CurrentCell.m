//
//  CurrentCell.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/16.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "CurrentCell.h"
#import "PackageModel.h"
#import <UIImageView+WebCache.h>

@interface CurrentCell ()

@property (weak, nonatomic) IBOutlet UIImageView *currentImage;

@property (weak, nonatomic) IBOutlet UILabel *cfdLab;

@property (weak, nonatomic) IBOutlet UILabel *jqqb;

@property (weak, nonatomic) IBOutlet UILabel *lisheng;

@property (weak, nonatomic) IBOutlet UILabel *chuyouNub;

@property (weak, nonatomic) IBOutlet UILabel *mamyiNub;


//block回调
@property (nonatomic, strong) void (^returnBackData) (NSDictionary * dic, NSInteger tag);



@end

@implementation CurrentCell


-(void)setModel:(PackageModel *)model
{
    _model = model;
    [_currentImage sd_setImageWithURL:[NSURL URLWithString:model.tp]];
    _cfdLab.text = model.cfd;
    _jqqb.text = model.jg;
    


}



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
