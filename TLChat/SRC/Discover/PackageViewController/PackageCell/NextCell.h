//
//  NextCell.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/16.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PackageModel.h"

@class PackageModel;
@interface NextCell : UICollectionViewCell

@property (nonatomic, strong) PackageModel *model;
@property (nonatomic, strong) UILabel * sectionLabel;

@end
