//
//  NextCell.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/16.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "NextCell.h"
#import "PackageModel.h"
#import <UIImageView+WebCache.h>

@interface NextCell()

@property (weak, nonatomic) IBOutlet UIImageView *NextImage;

@property (weak, nonatomic) IBOutlet UILabel *NextTitle;

@property (weak, nonatomic) IBOutlet UILabel *NextMs;

@property (weak, nonatomic) IBOutlet UILabel *Nextjq;






@end

@implementation NextCell

-(void)setModel:(PackageModel *)model
{
    _model = model;
    [_NextImage sd_setImageWithURL:[NSURL URLWithString:model.tp]];
    _NextTitle.text = model.bqlx;
    _NextMs.text = model.xlts;
    _Nextjq.text = model.jg;

}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    //[super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
