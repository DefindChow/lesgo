//
//  PackageDetailView.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/16.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PackageDetailView : UITableViewController

@property (nonatomic, strong) NSString * PackageID;

@end
