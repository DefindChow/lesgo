//
//  HeaderView.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/12.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HeaderViewDelegate <NSObject>

@optional
-(void)headViewDidSelected:(NSInteger)index;//显示更多

@end


@interface HeaderView : UITableViewHeaderFooterView

@property (nonatomic, copy) NSString *title;//设置组标题

@property (nonatomic, assign) NSInteger btnTag;//设置button的tag

@property (nonatomic, assign) id <HeaderViewDelegate> delegate;

@end
