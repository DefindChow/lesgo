//
//  HeaderView.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/12.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "HeaderView.h"
#import "TLMacros.h"

#define  HEIGHT 90

@interface HeaderView ()




@property (nonatomic, strong) UIButton *button;

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation HeaderView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createSubviews];
    }
    return self;
}



-(void)createSubviews
{
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, WIDTH_SCREEN - 90, HEIGHT)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textColor = [UIColor blackColor];
    _titleLabel.font = [UIFont systemFontOfSize:17];
    _titleLabel.textAlignment = NSTextAlignmentLeft;
    
    _button = [[UIButton alloc]initWithFrame:CGRectMake(_titleLabel.frame.size.width + 20, 0, 70,HEIGHT)];
    [_button setTitle:@"-- 开启愉快之旅 --" forState:UIControlStateNormal];
    [_button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];//lightGrayColor浅灰色
    _button.titleLabel.font = [UIFont systemFontOfSize:15];
    _button.titleLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_button];
    
    
    
}


-(void)buttonClicked:(UIButton *)button
{
    if (_delegate && [_delegate respondsToSelector:@selector(headViewDidSelected:)]) {
        
        [_delegate headViewDidSelected:button.tag];
    }
}

-(void)setTitle:(NSString *)title
{
    _title = title;
    _titleLabel.text = _title;
}

-(void)setBtnTag:(NSInteger)btnTag
{
    _button.tag = btnTag;//用来区分分组
}





@end

