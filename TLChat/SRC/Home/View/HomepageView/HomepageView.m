//
//  HomepageView.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/15.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "HomepageView.h"
#import "NewFrame.h"


@implementation HomepageView

{
    UIViewController *_viewController;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _viewController=[[UIViewController alloc] init];
        _viewController.view=self;
        [self showUI];
    }
    return self;
}


- (void)showUI{
    NSArray *images=@[@"3",@"4",@"5",@"6"];
    NSArray *titles=@[@"LES'GO", @"达人 show", @"社区", @"同城"];
    
    for (int i = 0; i < 4; i++) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = [UIView getBtnRectWithX:20 + 80 * i Y:360 width:39 height:39];//CGRectGetMaxY(sView.frame) + 10
        [button addTarget:self action:@selector(headButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = 100 + i;
        button.backgroundColor=[UIColor whiteColor];
        
        
        UILabel * label = [[UILabel alloc] init];
        label.frame = [UIView getBtnRectWithX:-2 Y:44 width:41 height:16];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont systemFontOfSize:14];
        label.text = titles[i];
        [button addSubview:label];
    }

}

- (void)headButtonClick:(UIButton *)button
{

    
}

@end
