//
//  HomepageView.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/15.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomepageViewClickedDelegate<NSObject>

- (void)click:(NSInteger)tag sex:(NSInteger)sex;

@end


@interface HomepageView : UIView

@property (nonatomic,retain)id<HomepageViewClickedDelegate>delegate;


@end
