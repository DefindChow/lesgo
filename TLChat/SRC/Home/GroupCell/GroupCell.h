//
//  GroupCell.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/9.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *GroupImage;
@property (weak, nonatomic) IBOutlet UILabel *GroupTitle;

@property (weak, nonatomic) IBOutlet UILabel *go;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BeginTime;

@property (weak, nonatomic) IBOutlet UILabel *EndTime;

@property (weak, nonatomic) IBOutlet UILabel *GroupLable;

@property (weak, nonatomic) IBOutlet UILabel *ZheKou;

@property (weak, nonatomic) IBOutlet UILabel *Money;

@property (weak, nonatomic) IBOutlet UILabel *RockPrice;



@end
