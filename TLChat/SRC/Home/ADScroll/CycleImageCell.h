//
//  CycleImageCell.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/19.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CycleImageCell : UICollectionViewCell

@property (nonatomic, copy) NSString *placeHolderImageName;

@property (nonatomic, strong) NSURL *imageURL;
@property (nonatomic, copy) NSString *imageName;
@property (nonatomic, strong) UIImageView *imageView;


@end
