//
//  CycleImageCell.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/19.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "CycleImageCell.h"
#import "UIImageView+WebCache.h"


@implementation CycleImageCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

-(void)createSubViews
{
    _imageView = [[UIImageView alloc]initWithFrame:self.bounds];
    [self.contentView addSubview:_imageView];
}

-(void)setPlaceHolderImageName:(NSString *)placeHolderImageName
{
    _placeHolderImageName = placeHolderImageName;
}

-(void)setImageURL:(NSURL *)imageURL
{
    _imageURL = imageURL;
    //实现异步加载图片，本地缓存，网络下载，设置占位图片
    [_imageView sd_setImageWithURL:_imageURL placeholderImage:[UIImage imageNamed:_placeHolderImageName]];
}

-(void)setImageName:(NSString *)imageName
{
    _imageName = imageName;
    _imageView.image = [UIImage imageNamed:_imageName];
}

@end
