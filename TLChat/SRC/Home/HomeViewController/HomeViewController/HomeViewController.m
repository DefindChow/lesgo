//
//  HomeViewController.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/15.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "HomeViewController.h"
#import "HomepageView.h"
#import "TLScanningViewController.h"
#import "ADScrollView.h"
#import "HeaderView.h"
#import "GroupCell.h"
#import "TarentoCell.h"
#import "ShequCell.h"
#import "UIButton+WebCache.h"
#import "TLMacros.h"


@interface HomeViewController ()<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>


@property (nonatomic, strong) UISearchBar *searchBar;//搜索
@property (nonatomic, strong) NSMutableArray * adArray;//广告数组
@property (nonatomic, strong) NSMutableArray *categoryArray;//分类数组
@property (nonatomic, strong) NSMutableArray *dataArray;//tableView的数据源数组
@property (nonatomic, strong) ADScrollView *AdView;

@property (nonatomic, strong) UIView *headerView;

@property (nonatomic, strong) UITableView *tableView;

//@property (nonatomic, strong) MBProgressHUD *HD;




@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createScrollView];//首页AD
    [self navigationItemSetting];
    // [self createSubView];
    self.navigationBarBackgroundHidden = YES;//(透明)隐藏导航栏

    
    
}


-(void)createUI
{
    if (_tableView) {
        return;
    }
    _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    //注册两个cell 并指定复用ID
    [_tableView registerNib:[UINib nibWithNibName:@"GroupCell" bundle:nil] forCellReuseIdentifier:@"GroupCell"];

    [_tableView registerNib:[UINib nibWithNibName:@"TarentoCell" bundle:nil] forCellReuseIdentifier:@"TarentoCell"];
    
    [_tableView registerNib:[UINib nibWithNibName:@"ShequCell" bundle:nil] forCellReuseIdentifier:@"ShequCell"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(64, 0, 49, 0));
    }];
    _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SIZE_SCREEN.width, (SIZE_SCREEN.height - 49) / 4 + 20 + ((SIZE_SCREEN.width - 150) / 4 + 30) * 2 ) ];
    _headerView.backgroundColor = [UIColor colorWithRed:228 / 255.0 green:229 / 255.0 blue:230 / 255.0 alpha:1];
    
    _AdView = [[ADScrollView alloc]initWithFrame:CGRectMake(0, 0, SIZE_SCREEN.width, (SIZE_SCREEN.height - 49-65)/4)];
    _AdView.infiniteLoop = YES;//是否循环滚动
    //pageControl的显示位置
    _AdView.pageControlPositionType = pageControlPositionTypeRight;
    //是否需要pageControl
    _AdView.needPageControl = YES;
    [_headerView addSubview:_AdView];
    
    _tableView.tableHeaderView = _headerView;
    
    //创建分类
    UIView *subView = [[UIView alloc]initWithFrame:CGRectMake(0, _AdView.frame.size.height + 10, SIZE_SCREEN.width, _AdView.frame.size.height + 50)];
    [_headerView addSubview:subView];
    _headerView.frame = CGRectMake(0, 0, SIZE_SCREEN.width, subView.frame.origin.y + subView.frame.size.height + 10);
    subView.backgroundColor = [UIColor whiteColor];

//    NSInteger i = 0;
//    CGFloat btnHeight = 60;
//    CGFloat btnWidth = (SIZE_SCREEN.width - 100)/4;
  
}


#pragma mark -导航条设置
-(void)navigationItemSetting
{
    UIButton *scanBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [scanBtn setImage:[UIImage imageNamed:@"扫一扫"] forState:UIControlStateNormal];
    [scanBtn addTarget:self action:@selector(scanBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:scanBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    //搜索条
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width - 50, 45)];
    _searchBar.delegate = self;
    //搜索条位置自适应
    [_searchBar sizeToFit];
    _searchBar.placeholder = @"输入目的地及好友";
    
    self.navigationItem.titleView = _searchBar;
    
    
}


/**扫一扫按钮事件*/
-(void)scanBtnClicked:(UIButton *)button
{
    TLScanningViewController *scanvc = [[TLScanningViewController alloc]init];
    self.hidesBottomBarWhenPushed = YES;//隐藏底部导航
    [self.navigationController pushViewController:scanvc animated:YES];
    self.hidesBottomBarWhenPushed = NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;//3组数据
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;//3行
}

//显示内容
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//
//    return cell;
//}

//选中某一行
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"选中了:%ld",indexPath.row);

}
//取消选中一行
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"取消选中了:%ld",indexPath.row);
    
}

- (void) setNavBarImg:(UINavigationBar *)navBar
{
#define kSCNavBarImageTag 10
    
    if ([navBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)])
    {
        //if iOS 5.0 and later
        
        [navBar setBackgroundImage:[UIImage imageNamed:@"bg_nav@2x"] forBarMetrics:UIBarMetricsDefault];
    }
    else
    {
        UIImageView *imageView = (UIImageView *)[navBar  viewWithTag:kSCNavBarImageTag];
        [imageView setBackgroundColor:[UIColor clearColor]];
        if (imageView == nil)
        {
            imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_nav@2x"]];
            [imageView setTag:kSCNavBarImageTag];
            [navBar insertSubview:imageView atIndex:0];
        }
    }
}



#pragma mark - UIScrollView
-(void)createScrollView
{
    
    UIScrollView *sv = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -64,WIDTH_SCREEN, 350)];
    
    sv.tag = 1;
    [self.view addSubview:sv];
    //添加子视图
    [self addImageViews];
}

-(void)addImageViews
{
    UIScrollView *sv = (id)[self.view viewWithTag:1];
    //设置包含子视图的大小
    sv.contentSize = CGSizeMake(self.view.bounds.size.width, 200);
    //设置按页滚动
    sv.pagingEnabled = YES;
    //设置禁止出界
    sv.bounces = NO;
    //遍历添加子视图
    for (int i = 0; i < 3; i++) {
        //获取到图片的名字
        NSString *imageName = [NSString stringWithFormat:@"banner%.2d", i + 1];
        NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
        //创建图片对象
        UIImage *image = [UIImage imageWithData:data];
        //每次循环都创建一个图片视图
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 200)];
        //设置图片
        imageView.image = image;
        //添加到sv上
        [sv addSubview:imageView];
    }
}


//AD
-(void)ScrollerView
{
   
    NSArray *imagePathArray = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpg" inDirectory:@""];
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,WIDTH_SCREEN, 200)];
    
    scroll.tag = 100;
    scroll.delegate = self;
    scroll.contentSize = CGSizeMake(self.view.bounds.size.width * imagePathArray.count, 200);
    scroll.pagingEnabled = YES;
    scroll.showsHorizontalScrollIndicator = NO;
    
    NSInteger i = 0;
    for (NSString * imageNamePath in imagePathArray) {
        NSArray * imageNameArray = [[imageNamePath lastPathComponent]componentsSeparatedByString:@"."];
        
        NSData * data = [[NSData alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageNameArray[0] ofType: imageNameArray[1]]];
        UIImage *image = [UIImage imageWithData:data];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width * i, 0, self.view.bounds.size.width, 200)];
        imageView.userInteractionEnabled = YES;
        imageView.image = image;
        i++;
        [scroll addSubview:imageView];
    }
    
    [self.view addSubview:scroll];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
