//
//  HomeViewController.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/15.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "BaseViewController.h"
#import "HomepageView.h"

@interface HomeViewController : BaseViewController <HomepageViewClickedDelegate>

@end
