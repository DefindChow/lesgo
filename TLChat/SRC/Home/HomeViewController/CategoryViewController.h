//
//  CategoryViewController.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/12.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "BaseViewController.h"

@interface CategoryViewController : BaseViewController

@property (nonatomic, copy) NSString *dataId;

@end
