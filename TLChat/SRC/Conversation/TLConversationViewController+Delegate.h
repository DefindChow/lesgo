//
//  TLConversationViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLConversationViewController.h"
#import "TLChatViewController.h"
#import "TLAddMenuView.h"

#define     HEIGHT_CONVERSATION_CELL        64.0f

@interface TLConversationViewController (Delegate) <TLMessageManagerConvVCDelegate, UISearchBarDelegate, TLAddMenuViewDelegate>

- (void)registerCellClass;

@end
