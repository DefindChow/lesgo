//
//  TLConversationCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLConversation.h"

@interface TLConversationCell : TLTableViewCell

/// 会话Model
@property (nonatomic, strong) TLConversation *conversation;

#pragma mark - Public Methods
/**
 *  标记为未读
 */
- (void) markAsUnread;

/**
 *  标记为已读
 */
- (void) markAsRead;

@end
