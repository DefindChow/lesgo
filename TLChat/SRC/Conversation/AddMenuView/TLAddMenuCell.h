//
//  TLAddMenuCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLAddMenuItem.h"

@interface TLAddMenuCell : TLTableViewCell

@property (nonatomic, strong) TLAddMenuItem *item;

@end
