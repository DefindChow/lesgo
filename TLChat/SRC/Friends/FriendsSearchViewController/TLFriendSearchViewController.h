//
//  TLFriendSearchViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewController.h"

#define     HEIGHT_FRIEND_CELL      54.0f

@interface TLFriendSearchViewController : TLTableViewController <UISearchResultsUpdating>

@property (nonatomic, strong) NSMutableArray *friendsData;

@end
