//
//  TLFriendDetailViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLFriendDetailViewController.h"
#import "TLFriendDetailUserCell.h"

#define     HEIGHT_USER_CELL           90.0f
#define     HEIGHT_ALBUM_CELL          80.0f

@interface TLFriendDetailViewController (Delegate) <TLFriendDetailUserCellDelegate>

- (void)registerCellClass;

@end
