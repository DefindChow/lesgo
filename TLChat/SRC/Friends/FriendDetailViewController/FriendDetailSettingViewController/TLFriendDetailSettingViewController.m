//
//  TLFriendDetailSettingViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLFriendDetailSettingViewController.h"
#import "TLFriendHelper+Detail.h"

@interface TLFriendDetailSettingViewController ()

@end

@implementation TLFriendDetailSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationItem setTitle:@"资料设置"];
    
    self.data = [[TLFriendHelper sharedFriendHelper] friendDetailSettingArrayByUserInfo:self.user];
}



@end
