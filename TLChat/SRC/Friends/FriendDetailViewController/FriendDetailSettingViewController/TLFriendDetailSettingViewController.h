//
//  TLFriendDetailSettingViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLSettingViewController.h"

@interface TLFriendDetailSettingViewController : TLSettingViewController

@property (nonatomic, strong) TLUser *user;

@end
