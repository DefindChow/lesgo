//
//  TLFriendDetailViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLInfoViewController.h"

@class TLUser;
@interface TLFriendDetailViewController : TLInfoViewController

@property (nonatomic, strong) TLUser *user;

@end
