//
//  TLFriendDetailUserCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLTableViewCell.h"
#import "TLInfo.h"

@protocol TLFriendDetailUserCellDelegate <NSObject>

- (void)friendDetailUserCellDidClickAvatar:(TLInfo *)info;

@end

@interface TLFriendDetailUserCell : TLTableViewCell

@property (nonatomic, assign) id<TLFriendDetailUserCellDelegate>delegate;

@property (nonatomic, strong) TLInfo *info;

@end
