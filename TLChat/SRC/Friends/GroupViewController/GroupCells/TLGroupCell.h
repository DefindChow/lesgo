//
//  TLGroupCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLTableViewCell.h"
#import "TLGroup.h"

@interface TLGroupCell : TLTableViewCell

@property (nonatomic, strong) TLGroup *group;

@end
