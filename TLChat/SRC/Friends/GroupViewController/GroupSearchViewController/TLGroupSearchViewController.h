//
//  TLGroupSearchViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewController.h"

@interface TLGroupSearchViewController : TLTableViewController <UISearchResultsUpdating, UISearchBarDelegate>

@property (nonatomic, strong) NSMutableArray *groupData;

@end
