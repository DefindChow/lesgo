//
//  TLGroupViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewController.h"
#import "TLGroupSearchViewController.h"

@interface TLGroupViewController : TLTableViewController

@property (nonatomic, strong) NSMutableArray *data;

@property (nonatomic, strong) TLGroupSearchViewController *searchVC;

@end
