//
//  TLFriendCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"

#import "TLUser.h"

@interface TLFriendCell : TLTableViewCell

/**
 *  用户信息
 */
@property (nonatomic, strong) TLUser *user;

@end
