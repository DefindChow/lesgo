//
//  TLFriendsViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLFriendsViewController.h"

#define     HEIGHT_FRIEND_CELL      54.0f
#define     HEIGHT_HEADER           22.0f

@interface TLFriendsViewController (Delegate) <UISearchBarDelegate>

- (void)registerCellClass;

@end
