//
//  TLContactsViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewController.h"
#import "TLContactsSearchViewController.h"

@interface TLContactsViewController : TLTableViewController

/// 通讯录好友（初始数据）
@property (nonatomic, strong) NSArray *contactsData;

/// 通讯录好友（格式化的列表数据）
@property (nonatomic, strong) NSArray *data;

/// 通讯录好友索引
@property (nonatomic, strong) NSArray *headers;

@property (nonatomic, strong) TLContactsSearchViewController *searchVC;

@end
