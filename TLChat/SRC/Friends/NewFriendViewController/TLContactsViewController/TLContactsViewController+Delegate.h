//
//  TLContactsViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLContactsViewController.h"

@interface TLContactsViewController (Delegate)

- (void)registerCellClass;

@end
