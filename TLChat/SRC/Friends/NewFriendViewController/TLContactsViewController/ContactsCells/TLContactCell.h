//
//  TLContactCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLContact.h"

/**
 *  通讯录 Cell
 */

@interface TLContactCell : TLTableViewCell

@property (nonatomic, strong) TLContact *contact;

@end
