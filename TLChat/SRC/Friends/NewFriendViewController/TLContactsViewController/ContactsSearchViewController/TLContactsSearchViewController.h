//
//  TLContactsSearchViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewController.h"

@interface TLContactsSearchViewController : TLTableViewController <UISearchResultsUpdating>

@property (nonatomic, strong) NSArray *contactsData;

@end
