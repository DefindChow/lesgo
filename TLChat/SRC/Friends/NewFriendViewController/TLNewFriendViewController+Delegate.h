//
//  TLNewFriendViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLNewFriendViewController.h"
#import "TLAddThirdPartFriendCell.h"

@interface TLNewFriendViewController (Delegate) <UISearchBarDelegate, TLAddThirdPartFriendCellDelegate>

- (void)registerCellClass;

@end
