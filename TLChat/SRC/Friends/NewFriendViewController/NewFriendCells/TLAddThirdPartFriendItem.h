//
//  TLAddThirdPartFriendItem.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLAddThirdPartFriendItem : UIButton

/**
 *  第三方类型
 *  {
 *      TLThirdPartFriendTypeContacts
 *      TLThirdPartFriendTypeQQ
 *      TLThirdPartFriendTypeGoogle
 *  }
 */
@property (nonatomic, strong) NSString *itemTag;

- (id)initWithImagePath:(NSString *)imagePath andTitle:(NSString *)title;

@end
