//
//  TLAddFriendViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLAddFriendViewController.h"
#import "EaseMob.h"

@interface TLAddFriendViewController ()<EMChatManagerDelegate>

//@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic, retain) UITextField *textField;


@end

@implementation TLAddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationItem setTitle:@"添加朋友"];
    //创建一个文本框视图
    [self createTextField];
    [self textFiledSetting];
    [self textSetting];
    [self keyboardSetting];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(self.view.width/4, 100, self.view.width / 2, 35)];
     button.backgroundColor = [UIColor orangeColor];
    
    //[button setBackgroundImage:[UIImage imageNamed:@"sousou"] forState:UIControlStateNormal
     [button setTitle:@"添加到通讯录" forState:UIControlStateNormal];
    
    
    //添加/注册事件
    [button addTarget:self action:@selector(addFriendAction) forControlEvents:UIControlEventTouchDown];
    //UIControlEventTouchDown
    //当按钮按下去,响应事件
    [self.view addSubview:button];
    //设置代理
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    
    
}
-(void)addFriendAction
{
    // 1.获取要添加好友的名字
    NSString *username = self.textField.text;
    
    NSString *loginUsername = [[EaseMob sharedInstance].chatManager loginInfo][@"username"];
    NSString *message = [@"我是" stringByAppendingString:loginUsername];
    
    EMError *error =  nil;
    [[EaseMob sharedInstance].chatManager addBuddy:username message:message error:&error];
    if (error) {
        NSLog(@"添加好友有问题 %@",error);
        
    }else{
        NSLog(@"添加好友没有问题");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)createTextField
{
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(0,15, self.view.width, 45)];
    textField.tag = 1;
    
    
    [self.view addSubview:textField];
}

-(void)textFiledSetting
{
    UITextField *textFiled = (id)[self.view viewWithTag:1];
    textFiled.borderStyle = UITextBorderStyleRoundedRect;

}

-(void)textSetting
{
    UITextField *textFiled = (id)[self.view viewWithTag:1];
    
    textFiled.placeholder = @"LES号/手机号";
    //设置文本框中文本的自适应
    textFiled.adjustsFontSizeToFitWidth = YES;
/*
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 40, 40)];
    imageView.image = [UIImage imageNamed:@"ss"];
    //设置文本框的右视图
    textFiled.rightView = imageView;
    textFiled.rightViewMode = UITextFieldViewModeAlways;
*/
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //收起键盘
    UITextField *textFiled = (id)[self.view viewWithTag:1];
    [textFiled resignFirstResponder];
    //称为第一响应resignFirstResponder
    //[textFiled becomeFirstResponder];
}
//自定制键盘
-(void)keyboardSetting
{
    UITextField *textFiled = (id)[self.view viewWithTag:1];

    //键盘的色彩
    textFiled.keyboardAppearance = UIKeyboardAppearanceDefault;

    textFiled.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    //要修改return健的样式,键盘样式要使用默认
    textFiled.returnKeyType =UIReturnKeySearch;
}


/*
- (IBAction)addFriendAction:(id)sender {
    
    // 1.获取要添加好友的名字
    NSString *username = self.textField.text;
    // 2.向服务器发送一个添加好友的请求
    NSString *loginUsername = [[EaseMob sharedInstance].chatManager loginInfo][@"username"];
    NSString *message = [@"我是" stringByAppendingString:loginUsername];
    
    EMError *error =  nil;
    [[EaseMob sharedInstance].chatManager addBuddy:username message:message error:&error];
    if (error) {
        NSLog(@"添加好友有问题 %@",error);
        
    }else{
        NSLog(@"添加好友没有问题");
    }
}
*/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
