//
//  TLTagCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"

@interface TLTagCell : TLTableViewCell

@property (nonatomic, strong) NSString *title;

@end
