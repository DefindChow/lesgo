//
//  TLTagsViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLTagsViewController.h"
#import "TLTagsViewController+Delegate.h"

@implementation TLTagsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationItem setTitle:@"星标好友"];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self registerCellClass];
    
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithTitle:@"新建" style:UIBarButtonItemStylePlain target:self action:@selector(rightBarButtonDown:)];
    [self.navigationItem setRightBarButtonItem:rightBarButton];
    
    self.data = [TLFriendHelper sharedFriendHelper].tagsData;
}

#pragma mark - Event Response -
- (void)rightBarButtonDown:(UIBarButtonItem *)sender
{
    
}

@end
