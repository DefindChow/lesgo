//
//  LESTool.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/10.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LESTool : NSObject

/**
 *  选择根控制器(主界面)
 */

+ (void)chooseRootController;

@end