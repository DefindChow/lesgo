//
//  LESTool.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/10.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "LESTool.h"
#import "GuidepageViewController.h"
#import "TLRootViewController.h"


@implementation LESTool

+(void)chooseRootController
{
     NSString *key = @"CFBundleVersion";
    
    // 取出沙盒中存储的上次使用软件的版本号
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults stringForKey:key];
    
    //获得当前软件版本号
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    NSLog(@"%@", currentVersion);
    
    if ([currentVersion isEqualToString:lastVersion]) {
        //显示状态栏
        [UIApplication sharedApplication].statusBarHidden = NO;
        [UIApplication sharedApplication].keyWindow.rootViewController = [[TLRootViewController alloc]init];
    } else {
    //引导页
        [UIApplication sharedApplication].keyWindow.rootViewController = [[GuidepageViewController alloc] init];
        // 存储新版本
        [defaults setObject:currentVersion forKey:key];
        [defaults synchronize];
  
    }
}

@end

