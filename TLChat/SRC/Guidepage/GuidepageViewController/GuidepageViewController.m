//
//  GuidepageViewController.m
//  LES'GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "GuidepageViewController.h"
#import "TLRootViewController.h"

#define GuidepageImageCount 4

@interface GuidepageViewController ()<UIScrollViewDelegate>
@property(nonatomic,weak)UIPageControl *pageControl;

@end

@implementation GuidepageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //1.添加UISrollView
    [self setupScrollView];
    //2.添加pageControl
    [self setupPageControl];

}

/**
 *  添加pageControl
 */
-(void)setupPageControl
{
    //1.添加
    UIPageControl *pageController = [[UIPageControl alloc]init];
    pageController.numberOfPages = GuidepageImageCount;
    CGFloat centerX = self.view.frame.size.width * 0.5;
    CGFloat centerY = self.view.frame.size.width -30;
    pageController.center = CGPointMake(centerX, centerY);
    pageController.bounds = CGRectMake(0, 0, 100, 30);
   // pageController.frame =  CGRectMake(self.view.size.width/2, 500, 200, 100);
    pageController.userInteractionEnabled = NO;
    [self.view addSubview:pageController];
    self.pageControl = pageController;
    //设置原点颜色   1,当前页所在点颜色  2,非当前页
    _pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:253 green:98 blue:42 alpha:1];
    _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:189/255.0 green:189/255.0 blue:189/255.0 alpha:1];
}

/**
 *  添加UISrollView
 */
-(void)setupScrollView
{
    UIScrollView *scrollView = [[UIScrollView alloc]init];
    scrollView.frame = self.view.bounds;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    //2.添加图片
    CGFloat imageW = scrollView.frame.size.width;
    CGFloat imageH = scrollView.frame.size.height;
    for (NSInteger index=0; index < GuidepageImageCount; index++) {
        UIImageView *imageView = [[UIImageView alloc]init];
        //设置图片
        NSString *name = [NSString stringWithFormat:@"WechatIMG%ld",index+1];
        imageView.image = [UIImage imageNamed:name];
        //设置frame
        CGFloat imageX = index * imageW;
        imageView.frame = CGRectMake(imageX, 0, imageW, imageH);
        [scrollView addSubview:imageView];
        //在最后一个图片上面添加按钮
        if (index == GuidepageImageCount - 1) {
            [self setupLastImageView:imageView];
        }
    }
    //    3.设置滚动内容的尺寸
    scrollView.contentSize = CGSizeMake(imageW * GuidepageImageCount, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    

}

//将内容添加到最后一个图片
-(void)setupLastImageView:(UIImageView *)imageView
{
    //0.imageView默认是不可点击的 将其设置为能跟用户交互
    imageView.userInteractionEnabled=YES;
    //1.添加开始按钮
    UIButton *startButton = [[UIButton alloc]init];
    [startButton setBackgroundImage:[UIImage imageNamed:@"WechatIMG5"] forState:UIControlStateNormal];
    //[startButton setBackgroundImage:[UIImage imageNamed:@"new_feature_finish_button_highlighted"] forState:UIControlStateHighlighted];
    //2.设置frame
    CGFloat centerX = imageView.frame.size.width * 0.5;
    CGFloat centerY = imageView.frame.size.height * 0.6;
    startButton.center = CGPointMake(centerX, centerY);
    startButton.bounds = (CGRect){CGPointZero,startButton.currentBackgroundImage.size};
    //3.设置文字
    [startButton setTitle:@" " forState:UIControlStateNormal];
    [startButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [startButton addTarget:self action:@selector(startButton:) forControlEvents:UIControlEventTouchUpInside];
    [imageView addSubview:startButton];
    
}

-(void)startButton:(UIButton *)button
{
    //显示状态栏
    [UIApplication sharedApplication].statusBarHidden=NO;
    //切换窗口的根控制器
    self.view.window.rootViewController = [[TLRootViewController alloc]init];
}

#pragma mark -scroll代理方法
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //1.取出水平方向上滚动的距离
    CGFloat offsetX=scrollView.contentOffset.x;
    //2.求出页码
    double pageDouble = offsetX / scrollView.frame.size.width;
    int pageInt = (int)(pageDouble+0.5);
    self.pageControl.currentPage = pageInt;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
