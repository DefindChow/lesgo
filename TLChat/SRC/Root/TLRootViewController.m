//
//  TLRootViewController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLRootViewController.h"

#import "HomeViewController.h"
#import "TLConversationViewController.h"
#import "TLFriendsViewController.h"//
#import "TLDiscoverViewController.h"
#import "TLMineViewController.h"
#import "MessageViewController.h"//

static TLRootViewController *rootVC = nil;

@interface TLRootViewController ()

@property (nonatomic, strong) NSArray *childVCArray;

@property (nonatomic, strong) HomeViewController *homeVC;
@property (nonatomic, strong) TLConversationViewController *conversationVC;
@property (nonatomic, strong) TLFriendsViewController *friendsVC;//
@property (nonatomic, strong) TLDiscoverViewController *discoverVC;
@property (nonatomic, strong) TLMineViewController *mineVC;
@property (nonatomic, strong) MessageViewController *messageVC;//

@end

@implementation TLRootViewController

+ (TLRootViewController *) sharedRootViewController
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        rootVC = [[TLRootViewController alloc] init];
    });
    return rootVC;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self setViewControllers:self.childVCArray];       // 初始化子控制器
}

- (id)childViewControllerAtIndex:(NSUInteger)index
{
    return [[self.childViewControllers objectAtIndex:index] rootViewController];
}

#pragma mark - Getters
- (NSArray *) childVCArray
{
    if (_childVCArray == nil) {
        TLNavigationController *homeNavC = [[TLNavigationController alloc]initWithRootViewController:self.homeVC];
        //TLNavigationController *messageNavC = [[TLNavigationController alloc]initWithRootViewController:self.messageVC];
        TLNavigationController *convNavC = [[TLNavigationController alloc] initWithRootViewController:self.conversationVC];
       // TLNavigationController *friendNavC = [[TLNavigationController alloc] initWithRootViewController:self.friendsVC];
        TLNavigationController *discoverNavC = [[TLNavigationController alloc] initWithRootViewController:self.discoverVC];
        TLNavigationController *mineNavC = [[TLNavigationController alloc] initWithRootViewController:self.mineVC];
        _childVCArray = @[homeNavC, convNavC, discoverNavC, mineNavC];
    }
    return _childVCArray;
}



-(HomeViewController *)homeVC
{
    if (_homeVC == nil) {
        _homeVC = [[HomeViewController alloc]init];
        [_homeVC.tabBarItem setTitle:@"首页"];
        [_homeVC.tabBarItem setImage:[UIImage imageNamed:@"1-1"]];
        [_homeVC.tabBarItem setSelectedImage:[UIImage imageNamed:@"1-1_press"]];
    }
    return _homeVC;
}


- (TLConversationViewController *) conversationVC
{
    if (_conversationVC == nil) {
        _conversationVC = [[TLConversationViewController alloc] init];
        [_conversationVC.tabBarItem setTitle:@"消息"];
        [_conversationVC.tabBarItem setImage:[UIImage imageNamed:@"tabbar_mainframe"]];
        [_conversationVC.tabBarItem setSelectedImage:[UIImage imageNamed:@"tabbar_mainframeHL"]];
    }
    return _conversationVC;
}


- (TLDiscoverViewController *) discoverVC
{
    if (_discoverVC == nil) {
        _discoverVC = [[TLDiscoverViewController alloc] init];
        [_discoverVC.tabBarItem setTitle:@"发现"];
        [_discoverVC.tabBarItem setImage:[UIImage imageNamed:@"1-3"]];
        [_discoverVC.tabBarItem setSelectedImage:[UIImage imageNamed:@"1-3_press"]];
    }
    return _discoverVC;
}

- (TLMineViewController *) mineVC
{
    if (_mineVC == nil) {
        _mineVC = [[TLMineViewController alloc] init];
        [_mineVC.tabBarItem setTitle:@"我的"];
        [_mineVC.tabBarItem setImage:[UIImage imageNamed:@"1-4"]];
        [_mineVC.tabBarItem setSelectedImage:[UIImage imageNamed:@"1-4_press"]];
    }
    return _mineVC;
}

@end
