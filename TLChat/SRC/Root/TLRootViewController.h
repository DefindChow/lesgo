//
//  TLRootViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTabBarController.h"

@interface TLRootViewController : TLTabBarController

+ (TLRootViewController *) sharedRootViewController;

/**
 *  获取tabbarController的第Index个VC（不是navController）
 *
 *  @return navController的rootVC
 */
- (id)childViewControllerAtIndex:(NSUInteger)index;

@end
