//
//  TLChatCellMenuView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, TLChatMenuItemType) {
    TLChatMenuItemTypeCancel,
    TLChatMenuItemTypeCopy,
    TLChatMenuItemTypeDelete,
};

@interface TLChatCellMenuView : UIView

@property (nonatomic, assign, readonly) BOOL isShow;

@property (nonatomic, assign) TLMessageType messageType;

@property (nonatomic, copy) void (^actionBlcok)();

+ (TLChatCellMenuView *)sharedMenuView;

- (void)showInView:(UIView *)view withMessageType:(TLMessageType)messageType rect:(CGRect)rect actionBlock:(void (^)(TLChatMenuItemType))actionBlock;

- (void)dismiss;

@end
