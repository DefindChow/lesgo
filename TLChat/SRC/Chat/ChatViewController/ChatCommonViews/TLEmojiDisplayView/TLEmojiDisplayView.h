//
//  TLEmojiDisplayView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLEmoji.h"

@interface TLEmojiDisplayView : UIImageView

@property (nonatomic, strong) TLEmoji *emoji;

@property (nonatomic, assign) CGRect rect;

- (void)displayEmoji:(TLEmoji *)emoji atRect:(CGRect)rect;

@end
