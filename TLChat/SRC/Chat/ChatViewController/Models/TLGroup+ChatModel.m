
//
//  TLGroup+ChatModel.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLGroup+ChatModel.h"

@implementation TLGroup (ChatModel)

- (NSString *)chat_userID
{
    return self.groupID;
}

- (NSString *)chat_username
{
    return self.groupName;
}

- (NSString *)chat_avatarURL
{
    return nil;
}

- (NSString *)chat_avatarPath
{
    return self.groupAvatarPath;
}

- (NSInteger)chat_userType
{
    return TLChatUserTypeGroup;
}

- (id)groupMemberByID:(NSString *)userID
{
    return [self memberByUserID:userID];
}

- (NSArray *)groupMembers
{
    return self.users;
}

@end
