//
//  TLGroup+ChatModel.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLGroup.h"
#import "TLChatUserProtocol.h"

@interface TLGroup (ChatModel) <TLChatUserProtocol>

@end
