//
//  TLChatBaseViewController+ChatTableView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLChatBaseViewController.h"

#define     MAX_SHOWTIME_MSG_COUNT      10
#define     MAX_SHOWTIME_MSG_SECOND     30

@interface TLChatBaseViewController (ChatTableView) <TLChatTableViewControllerDelegate>

/**
 *  展示消息（添加到chatVC）
 */
- (void)addToShowMessage:(TLMessage *)message;

- (void)resetChatTVC;

@end
