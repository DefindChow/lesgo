//
//  TLChatViewControllerProxy.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TLImageMessage;
@protocol TLChatViewControllerProxy <NSObject>

@optional;
- (void)didClickedUserAvatar:(TLUser *)user;

- (void)didClickedImageMessages:(NSArray *)imageMessages atIndex:(NSInteger)index;

@end
