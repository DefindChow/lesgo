//
//  TLChatBaseViewController+DataDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLChatBaseViewController.h"

@interface TLChatBaseViewController (DataDelegate)

/**
 *  发送消息
 */
- (void)sendMessage:(TLMessage *)message;

@end
