//
//  TLMoreKeyboard+CollectionViewDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMoreKeyboard.h"

@interface TLMoreKeyboard (CollectionViewDelegate) <UICollectionViewDataSource, UICollectionViewDelegate>

- (void)registerCellClass;

@end
