//
//  TLMoreKeyboardCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMoreKeyboardItem.h"

@interface TLMoreKeyboardCell : UICollectionViewCell

@property (nonatomic, strong) TLMoreKeyboardItem *item;

@property (nonatomic, strong) void(^clickBlock)(TLMoreKeyboardItem *item);

@end