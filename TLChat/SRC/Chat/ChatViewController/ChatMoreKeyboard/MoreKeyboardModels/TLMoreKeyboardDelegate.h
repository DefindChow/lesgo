//
//  TLMoreKeyboardDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLMoreKeyboardItem.h"

@protocol TLMoreKeyboardDelegate <NSObject>
@optional
- (void) moreKeyboard:(id)keyboard didSelectedFunctionItem:(TLMoreKeyboardItem *)funcItem;

@end
