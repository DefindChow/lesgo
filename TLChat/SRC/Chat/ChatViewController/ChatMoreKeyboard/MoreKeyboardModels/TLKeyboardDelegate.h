//
//  TLKeyboardDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TLKeyboardDelegate <NSObject>

@optional
- (void) chatKeyboardWillShow:(id)keyboard;

- (void) chatKeyboardDidShow:(id)keyboard;

- (void) chatKeyboardWillDismiss:(id)keyboard;

- (void) chatKeyboardDidDismiss:(id)keyboard;

- (void) chatKeyboard:(id)keyboard didChangeHeight:(CGFloat)height;

@end
