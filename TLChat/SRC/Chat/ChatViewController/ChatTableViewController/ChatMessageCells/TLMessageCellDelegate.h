
//
//  TLMessageCellDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TLChatUserProtocol;
@class TLMessage;
@protocol TLMessageCellDelegate <NSObject>

- (void)messageCellDidClickAvatarForUser:(id<TLChatUserProtocol>)user;

- (void)messageCellTap:(TLMessage *)message;

- (void)messageCellLongPress:(TLMessage *)message rect:(CGRect)rect;

- (void)messageCellDoubleClick:(TLMessage *)message;

@end
