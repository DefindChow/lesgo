//
//  TLChatTableViewController+Delegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLChatTableViewController.h"
#import "TLTextMessageCell.h"
#import "TLImageMessageCell.h"
#import "TLExpressionMessageCell.h"

@interface TLChatTableViewController (Delegate) <TLMessageCellDelegate, TLActionSheetDelegate>

- (void)registerCellClass;

@end
