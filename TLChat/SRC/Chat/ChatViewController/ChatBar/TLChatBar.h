//
//  TLChatBar.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLChatBarDelegate.h"

@interface TLChatBar : UIView

@property (nonatomic, assign) id<TLChatBarDelegate> delegate;

@property (nonatomic, assign) id<TLChatBarDataDelegate> dataDelegate;

@property (nonatomic, assign) TLChatBarStatus status;

@property (nonatomic, strong, readonly) NSString *curText;

@property (nonatomic, assign) BOOL activity;

- (void)addEmojiString:(NSString *)emojiString;

- (void)sendCurrentText;

@end
