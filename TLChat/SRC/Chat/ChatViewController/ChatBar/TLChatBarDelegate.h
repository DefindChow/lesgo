//
//  TLChatBarDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "TLChatMacros.h"

@class TLChatBar;
@protocol TLChatBarDelegate <NSObject>

/**
 *  chatBar状态改变
 */
- (void)chatBar:(TLChatBar *)chatBar changeStatusFrom:(TLChatBarStatus)fromStatus to:(TLChatBarStatus)toStatus;

/**
 *  输入框高度改变
 */
- (void)chatBar:(TLChatBar *)chatBar didChangeTextViewHeight:(CGFloat)height;

@end



@protocol TLChatBarDataDelegate <NSObject>
/**
 *  发送文字
 */
- (void)chatBar:(TLChatBar *)chatBar sendText:(NSString *)text;


// 录音控制 
- (void)chatBarRecording:(TLChatBar *)chatBar;

- (void)chatBarWillCancelRecording:(TLChatBar *)chatBar;

- (void)chatBarDidCancelRecording:(TLChatBar *)chatBar;

- (void)chatBarFinishedRecoding:(TLChatBar *)chatBar;

@end