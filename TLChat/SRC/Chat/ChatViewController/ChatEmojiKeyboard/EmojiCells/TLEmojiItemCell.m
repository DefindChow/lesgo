//
//  TLEmojiItemCell.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLEmojiItemCell.h"

@interface TLEmojiItemCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation TLEmojiItemCell

- (id) initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self.contentView addSubview:self.label];
        [self p_addMasonry];
    }
    return self;
}

- (void)setEmojiItem:(TLEmoji *)emojiItem
{
    [super setEmojiItem:emojiItem];
    [self.label setText:emojiItem.emojiName];
}

#pragma mark - Private Methods -
- (void)p_addMasonry
{
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self);
    }];
}

#pragma mark - Getter -
- (UILabel *)label
{
    if (_label == nil) {
        _label = [[UILabel alloc] init];
        [_label setFont:[UIFont systemFontOfSize:25.0f]];
    }
    return _label;
}

@end
