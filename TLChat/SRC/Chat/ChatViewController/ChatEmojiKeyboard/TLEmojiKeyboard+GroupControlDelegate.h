//
//  TLEmojiKeyboard+GroupControlDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLEmojiKeyboard.h"

@interface TLEmojiKeyboard (GroupControlDelegate) <TLEmojiGroupControlDelegate>

- (void)updateSendButtonStatus;

@end
