//
//  TLEmojiGroupCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLEmojiGroup.h"

@interface TLEmojiGroupCell : UICollectionViewCell

@property (nonatomic, strong) TLEmojiGroup *emojiGroup;

@end
