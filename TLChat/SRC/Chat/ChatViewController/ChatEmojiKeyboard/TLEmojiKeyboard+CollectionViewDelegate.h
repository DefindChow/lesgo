//
//  TLEmojiKeyboard+CollectionViewDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLEmojiKeyboard.h"

#define     HEIGHT_TOP_SPACE            10
#define     HEIGHT_EMOJIVIEW            (HEIGHT_CHAT_KEYBOARD * 0.75 - HEIGHT_TOP_SPACE)
#define     HEIGHT_PAGECONTROL          HEIGHT_CHAT_KEYBOARD * 0.1
#define     HEIGHT_GROUPCONTROL         HEIGHT_CHAT_KEYBOARD * 0.17

@interface TLEmojiKeyboard (CollectionViewDelegate) <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

- (void)resetCollectionSize;

@end
