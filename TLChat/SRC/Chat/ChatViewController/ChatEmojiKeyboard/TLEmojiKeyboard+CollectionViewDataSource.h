//
//  TLEmojiKeyboard+CollectionViewDataSource.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLEmojiKeyboard.h"
#import "TLEmojiItemCell.h"
#import "TLEmojiFaceItemCell.h"
#import "TLEmojiImageItemCell.h"
#import "TLEmojiImageTitleItemCell.h"

@interface TLEmojiKeyboard (CollectionViewDataSource) <UICollectionViewDataSource>

- (void)registerCellClass;

- (NSUInteger)transformCellIndex:(NSInteger)index;

- (NSUInteger)transformModelIndex:(NSInteger)index;

@end
