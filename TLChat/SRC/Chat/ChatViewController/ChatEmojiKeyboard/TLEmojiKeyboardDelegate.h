//
//  TLEmojiKeyboardDelegate.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLEmoji.h"

@class TLEmojiKeyboard;
@protocol TLEmojiKeyboardDelegate <NSObject>

- (BOOL)chatInputViewHasText;

@optional
- (void)emojiKeyboard:(TLEmojiKeyboard *)emojiKB didTouchEmojiItem:(TLEmoji *)emoji atRect:(CGRect)rect;

- (void)emojiKeyboardCancelTouchEmojiItem:(TLEmojiKeyboard *)emojiKB;

- (void)emojiKeyboard:(TLEmojiKeyboard *)emojiKB didSelectedEmojiItem:(TLEmoji *)emoji;

- (void)emojiKeyboardSendButtonDown;

- (void)emojiKeyboardEmojiEditButtonDown;

- (void)emojiKeyboardMyEmojiEditButtonDown;

- (void)emojiKeyboard:(TLEmojiKeyboard *)emojiKB selectedEmojiGroupType:(TLEmojiType)type;

@end
