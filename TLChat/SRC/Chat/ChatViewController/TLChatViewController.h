//
//  TLChatViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLChatBaseViewController.h"

@interface TLChatViewController : TLChatBaseViewController

+ (TLChatViewController *) sharedChatVC;

@end
