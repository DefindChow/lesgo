//
//  TLUserGroupCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TLUserGroupCellDelegate <NSObject>

- (void)userGroupCellDidSelectUser:(TLUser *)user;

- (void)userGroupCellAddUserButtonDown;

@end

@interface TLUserGroupCell : UITableViewCell

@property (nonatomic, assign) id<TLUserGroupCellDelegate>delegate;

@property (nonatomic, strong) NSMutableArray *users;


@end
