//
//  TLUserGroupItemCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLUserGroupItemCell : UICollectionViewCell

@property (nonatomic, strong) TLUser *user;

@property (nonatomic, strong) void (^clickBlock)(TLUser *user);

@end
