//
//  TLChatGroupDetailViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLSettingViewController.h"
#import "TLGroup.h"

@interface TLChatGroupDetailViewController : TLSettingViewController

@property (nonatomic, strong) TLGroup *group;

@end
