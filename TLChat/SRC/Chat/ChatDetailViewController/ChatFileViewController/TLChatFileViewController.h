//
//  TLChatFileViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLViewController.h"

@interface TLChatFileViewController : TLViewController

@property (nonatomic, strong) NSString *partnerID;

@property (nonatomic, strong) NSMutableArray *data;

@end
