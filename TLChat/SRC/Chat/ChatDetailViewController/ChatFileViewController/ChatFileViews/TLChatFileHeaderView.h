//
//  TLChatFileHeaderView.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLChatFileHeaderView : UICollectionReusableView

@property (nonatomic, strong) NSString *title;

@end
