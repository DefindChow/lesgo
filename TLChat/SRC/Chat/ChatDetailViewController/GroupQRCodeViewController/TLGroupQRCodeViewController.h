//
//  TLGroupQRCodeViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLViewController.h"
#import "TLGroup.h"

@interface TLGroupQRCodeViewController : TLViewController

@property (nonatomic, strong) TLGroup *group;

@end
