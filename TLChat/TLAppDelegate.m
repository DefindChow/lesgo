//
//  TLAppDelegate.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLAppDelegate.h"
#import "TLRootViewController.h"

#import "EaseMob.h"

#import "TLRootProxy.h"
#import "TLExpressionProxy.h"
#import "TLExpressionHelper.h"

#import <AFNetworking.h>
#import <JSPatch/JSPatch.h>
//#import <SMS_SDK/SMSSDK.h>
#import "LESTool.h"
#import "GuidepageViewController.h"

@interface TLAppDelegate ()<EMChatManagerDelegate>

@end

@implementation TLAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self p_initThirdPartSDK];      // 初始化第三方SDK
    [self p_initAppData];           // 初始化应用信息
//    [self p_initUserData];          // 初始化用户信息
    [self p_initUI];                // 初始化UI
//    [self firstStr];
    [self p_urgentMethod];          // 紧急方法
    [LESTool chooseRootController];
    
    //registerSDKWithAppKey: 注册的AppKey，详细见下面注释。
    //apnsCertName: 推送证书名（不需要加后缀），详细见下面注释。
    [[EaseMob sharedInstance] registerSDKWithAppKey:@"zeersi#lesgo" apnsCertName:nil otherConfig:@{kSDKConfigEnableConsoleLogger:@(NO)}];
//@"istore_dev"
    [[EaseMob sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {

}
// APP进入后台
- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    [[EaseMob sharedInstance] applicationDidEnterBackground:application];
    
}

// APP将要从后台返回
- (void)applicationWillEnterForeground:(UIApplication *)application {
    
    [[EaseMob sharedInstance] applicationWillEnterForeground:application];
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

// 申请处理时间
- (void)applicationWillTerminate:(UIApplication *)application {
    
     [[EaseMob sharedInstance] applicationWillTerminate:application];
    
}

#pragma mark - Private Methods
- (void)p_initThirdPartSDK
{
    // 友盟统计
    [MobClick startWithAppkey:UMENG_APPKEY reportPolicy:BATCH channelId:APP_CHANNEL];//UMENG_APPKEY---APP_CHANNEL
    
    // 网络环境监测
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    
    // JSPatch
#ifdef DEBUG_JSPATCH
    [JSPatch testScriptInBundle];
#else
    [JSPatch startWithAppKey:JSPATCH_APPKEY];
    [JSPatch sync];
#endif
    
    // Mob SMS
    // [SMSSDK registerApp:MOB_SMS_APPKEY withSecret:MOB_SMS_SECRET];

    // 提示框
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
    
    // 日志
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    DDFileLogger *fileLogger = [[DDFileLogger alloc] init];
    fileLogger.rollingFrequency = 60 * 60 * 24;
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    [DDLog addLogger:fileLogger];
}

- (void)p_initUI
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    TLRootViewController *rootVC = [TLRootViewController sharedRootViewController];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.window setRootViewController:rootVC];
    [self.window addSubview:rootVC.view];
    [self.window makeKeyAndVisible];
    
   
}




- (void)p_initAppData
{
    TLRootProxy *proxy = [[TLRootProxy alloc] init];
    [proxy requestClientInitInfoSuccess:^(id data) {
        
    } failure:^(NSString *error) {
        
    }];
}

- (void)p_urgentMethod
{
    // 由JSPatch重写
}



@end
