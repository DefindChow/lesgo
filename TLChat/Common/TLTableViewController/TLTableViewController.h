//
//  TLTableViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TLTableViewController : UITableViewController<UITableViewDataSource, UITableViewDelegate>


@property (nonatomic, strong) NSString *analyzeTitle;

@property (nonatomic, strong) UITableView *gttableView;

@end
