//
//  BaseRequest.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/18.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseRequest : NSObject

/**POST 请求方式*/
+(void)posetWithURL:(NSString *)url parameters:(NSDictionary *)para callBack:(void(^)(id data,NSError *error))callBack;

@end
