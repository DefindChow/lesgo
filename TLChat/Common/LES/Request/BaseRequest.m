//
//  BaseRequest.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/18.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "BaseRequest.h"
#import "AFNetworking/AFNetworking.h"

@implementation BaseRequest

//POST请求
+(void)posetWithURL:(NSString *)url parameters:(NSDictionary *)para callBack:(void (^)(id, NSError *))callBack
{
    //NSURLSession的封装
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //设置响应解析器(二进制)
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager POST:url parameters:para success:^(NSURLSessionDataTask *task, id responseObject) {
        //将请求结果反馈给请求者
        callBack(responseObject,nil);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        callBack(nil,error);
    }];
}




@end
