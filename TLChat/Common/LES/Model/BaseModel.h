//
//  BaseModel.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/21.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "JSONModel.h"
#import "BaseRequest.h"
#import "APPHeader.h"

@interface BaseModel : JSONModel

@end
