//
//  TLTabBarController.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTabBarController.h"

@implementation TLTabBarController

- (void) viewDidLoad
{
    [super viewDidLoad];

    [self.tabBar setBackgroundColor:[UIColor colorGrayBG]];//colorGrayBG
    [self.tabBar setTintColor:[UIColor colorGreenDefault]];//colorGreenDefault 绿色
}

@end
