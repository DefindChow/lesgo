//
//  TLInfoHeaderFooterView.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLInfoHeaderFooterView.h"

@implementation TLInfoHeaderFooterView

- (id) initWithReuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        [self.contentView setBackgroundColor:[UIColor colorGrayBG]];
    }
    return self;
}

@end
