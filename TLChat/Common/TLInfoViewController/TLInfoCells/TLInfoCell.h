//
//  TLInfoCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLInfo.h"

@interface TLInfoCell : TLTableViewCell

@property (nonatomic, strong) TLInfo *info;

@end
