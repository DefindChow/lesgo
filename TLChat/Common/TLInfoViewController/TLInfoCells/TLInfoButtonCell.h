//
//  TLInfoButtonCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLTableViewCell.h"
#import "TLInfo.h"

@protocol TLInfoButtonCellDelegate <NSObject>

- (void)infoButtonCellClicked:(TLInfo *)info;

@end

@interface TLInfoButtonCell : TLTableViewCell

@property (nonatomic, assign) id<TLInfoButtonCellDelegate>delegate;

@property (nonatomic, strong) TLInfo *info;

@end
