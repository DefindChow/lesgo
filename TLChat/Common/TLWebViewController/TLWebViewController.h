//
//  TLWebViewController.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface TLWebViewController : UIViewController <WKNavigationDelegate>

/// 是否使用网页标题作为nav标题，默认YES
@property (nonatomic, assign) BOOL useMPageTitleAsNavTitle;

/// 是否显示加载进度，默认YES
@property (nonatomic, assign) BOOL showLoadingProgress;

// 是否禁止历史记录，默认NO
@property (nonatomic, assign) BOOL disableBackButton;

@property (nonatomic, strong) NSString *url;

@end
