//
//  TLSettingSwitchCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLSettingItem.h"

@protocol TLSettingSwitchCellDelegate <NSObject>
@optional
- (void)settingSwitchCellForItem:(TLSettingItem *)settingItem didChangeStatus:(BOOL)on;
@end

@interface TLSettingSwitchCell : UITableViewCell

@property (nonatomic, assign) id<TLSettingSwitchCellDelegate>delegate;

@property (nonatomic, strong) TLSettingItem *item;

@end
