//
//  TLMenuItem.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

#define     TLCreateMenuItem(IconPath, Title) [TLMenuItem createMenuWithIconPath:IconPath title:Title]

@interface TLMenuItem : NSObject

/**
 *  左侧图标路径
 */
@property (nonatomic, strong) NSString *iconPath;

/**
 *  标题
 */
@property (nonatomic, strong) NSString *title;

/**
 *  副标题
 */
@property (nonatomic, strong) NSString *subTitle;

/**
 *  副图片URL
 */
@property (nonatomic, strong) NSString *rightIconURL;

/**
 *  是否显示红点
 */
@property (nonatomic, assign) BOOL showRightRedPoint;

+ (TLMenuItem *) createMenuWithIconPath:(NSString *)iconPath title:(NSString *)title;

@end
