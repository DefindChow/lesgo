//
//  TLMenuCell.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLMenuItem.h"

@interface TLMenuCell : UITableViewCell

@property (nonatomic, strong) TLMenuItem *menuItem;

@end
