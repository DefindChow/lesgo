//
//  TLMenuItem.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMenuItem.h"

@implementation TLMenuItem

+ (TLMenuItem *) createMenuWithIconPath:(NSString *)iconPath title:(NSString *)title
{
    TLMenuItem *item = [[TLMenuItem alloc] init];
    item.iconPath = iconPath;
    item.title = title;
    return item;
}

@end
