//
//  TLFriendHelper+Detail.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLFriendHelper.h"

@interface TLFriendHelper (Detail)

- (NSMutableArray *)friendDetailArrayByUserInfo:(TLUser *)userInfo;

- (NSMutableArray *)friendDetailSettingArrayByUserInfo:(TLUser *)userInfo;

@end
