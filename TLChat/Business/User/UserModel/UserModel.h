//
//  UserModel.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/18.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol UserModel <NSObject>



@end

@interface UserModel : NSObject

@property (nonatomic, copy) NSString *tx;//头像
@property (nonatomic, retain) NSString *les;//id
@property (nonatomic, copy) NSString *nc;//昵称
@property (nonatomic, copy) NSString *xb;//性别
@property (nonatomic, copy) NSString *diqu;//地区
@property (nonatomic, retain) NSString *gqian;//个签
@property (nonatomic, retain) NSString *mp;//二维码




@end
