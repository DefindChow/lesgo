//
//  TLMessageManager+ConversationRecord.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLMessageManager.h"

@interface TLMessageManager (ConversationRecord)

- (BOOL)addConversationByMessage:(TLMessage *)message;

- (void)conversationRecord:(void (^)(NSArray *))complete;

- (BOOL)deleteConversationByPartnerID:(NSString *)partnerID;

@end
