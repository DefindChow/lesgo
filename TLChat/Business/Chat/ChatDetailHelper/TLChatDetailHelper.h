//
//  TLChatDetailHelper.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLSettingGroup.h"
#import "TLGroup.h"

@interface TLChatDetailHelper : NSObject

- (NSMutableArray *)chatDetailDataByUserInfo:(TLUser *)userInfo;

- (NSMutableArray *)chatDetailDataByGroupInfo:(TLGroup *)groupInfo;

@end
