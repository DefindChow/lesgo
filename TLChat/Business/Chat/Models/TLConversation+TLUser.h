//
//  TLConversation+TLUser.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLConversation.h"
#import "TLUser+ChatModel.h"
#import "TLGroup+ChatModel.h"

@interface TLConversation (TLUser)

- (void)updateUserInfo:(TLUser *)user;

- (void)updateGroupInfo:(TLGroup *)group;

@end
