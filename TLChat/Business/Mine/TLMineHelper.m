//
//  TLMineHelper.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMineHelper.h"
#import "TLMenuItem.h"

@implementation TLMineHelper

- (id) init
{
    if (self = [super init]) {
        self.mineMenuData = [[NSMutableArray alloc] init];
        [self p_initTestData];
    }
    return self;
}


- (void) p_initTestData
{
    TLMenuItem *item0 = TLCreateMenuItem(nil, nil);
    TLMenuItem *item1 = TLCreateMenuItem(@"63", @"个人动态");
    TLMenuItem *item2 = TLCreateMenuItem(@"64", @"我的认证");

    TLMenuItem *item3 = TLCreateMenuItem(@"68", @"通讯录");
    TLMenuItem *item4 = TLCreateMenuItem(@"mine_setting", @"设置");
    [self.mineMenuData addObjectsFromArray:@[@[item0], @[item1, item2], @[item3], @[item4]]];

}

@end
