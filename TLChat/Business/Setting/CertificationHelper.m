//
//  CertificationHelper.m
//  LES'GO
//
//  Created by 刘超锦 on 16/8/25.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "CertificationHelper.h"

@implementation CertificationHelper




- (id) init
{
    if (self = [super init]) {
        self.CertificationData = [[NSMutableArray alloc] init];
        [self p_initTestData];
    }
    return self;
}

- (void) p_initTestData
{
    
    TLSettingItem *item1 = TLCreateSettingItem(@"身份证认证");
    TLSettingGroup *group1 = TLCreateSettingGroup(nil, nil, @[item1]);
    
    
    [self.CertificationData addObjectsFromArray:@[group1]];
}







@end
