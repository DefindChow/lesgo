//
//  CertificationHelper.h
//  LES'GO
//
//  Created by 刘超锦 on 16/8/25.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLSettingGroup.h"

@interface CertificationHelper : NSObject

@property (nonatomic, strong) NSMutableArray *CertificationData;

@end
