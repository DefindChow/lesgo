//
//  TLCommonSettingHelper.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TLSettingGroup.h"

@interface TLCommonSettingHelper : NSObject

@property (nonatomic, strong) NSMutableArray *commonSettingData;

+ (NSMutableArray *)chatBackgroundSettingData;

@end
