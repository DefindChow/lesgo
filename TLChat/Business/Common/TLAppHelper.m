//
//  TLAppHelper.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLAppHelper.h"

static TLAppHelper *helper;

@implementation TLAppHelper

+ (TLAppHelper *)sharedHelper
{
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        helper = [[TLAppHelper alloc] init];
    });
    return helper;
}

- (NSString *)version
{
    if (_version == nil) {
        NSString *shortVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        NSString *buildID = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
        _version = [NSString stringWithFormat:@"%@ (%@)", shortVersion, buildID];
    }
    return _version;
}

@end
