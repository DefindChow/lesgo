//
//  TLAppHelper.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLAppHelper : NSObject

@property (nonatomic, strong) NSString *version;

+ (TLAppHelper *)sharedHelper;

@end
