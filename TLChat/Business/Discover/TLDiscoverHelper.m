//
//  TLDiscoverHelper.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLDiscoverHelper.h"
#import "TLMenuItem.h"

@implementation TLDiscoverHelper

- (id) init
{
    if (self = [super init]) {
        self.discoverMenuData = [[NSMutableArray alloc] init];
        [self p_initTestData];
    }
    return self;
}

- (void) p_initTestData
{
    TLMenuItem *item1 = TLCreateMenuItem(@"56", @"朋友圈");//discover_album
    item1.rightIconURL = @"http://img4.duitang.com/uploads/item/201510/16/20151016113134_TZye4.thumb.224_0.jpeg";
    item1.showRightRedPoint = YES;
    TLMenuItem *item2 = TLCreateMenuItem(@"57", @"跟团游");
    TLMenuItem *item3 = TLCreateMenuItem(@"58", @"自由行");
    TLMenuItem *item4 = TLCreateMenuItem(@"discover_QRcode", @"扫一扫");
    TLMenuItem *item5 = TLCreateMenuItem(@"60", @"附近");//discover_location
    TLMenuItem *item6 = TLCreateMenuItem(@"61", @"社区");

    
    [self.discoverMenuData addObjectsFromArray:@[@[item1], @[item2, item3], @[item4, item5],@[item6]]];
}

@end
