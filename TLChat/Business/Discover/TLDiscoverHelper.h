//
//  TLDiscoverHelper.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLDiscoverHelper : NSObject

@property (nonatomic, strong) NSMutableArray *discoverMenuData;

@end
