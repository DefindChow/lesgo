//
//  UIButton+TLChat.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (TLChat)

- (void) setImage:(UIImage *)image imageHL:(UIImage *)imageHL;

@end
