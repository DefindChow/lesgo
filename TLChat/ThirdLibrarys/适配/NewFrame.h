//
//  NewFrame.h
//  PlayInBeijing
//
//  Created by MS on 15-9-23.
//  Copyright (c) 2015年 MS. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllDevice.h"

@interface UIView (NewFrame)

+ (CGRect)getRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;

+ (CGRect)getBtnRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height;

@end
