//
//  NewFrame.m
//  PlayInBeijing
//
//  Created by MS on 15-9-23.
//  Copyright (c) 2015年 MS. All rights reserved.
//

#import "NewFrame.h"

@implementation UIView (NewFrame)

//以4s为基准
+ (CGRect)getRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height
{
    CGRect rect = CGRectZero;
    CGFloat height4s = 480.;
    CGFloat width4s = 320.;
    if (IPHONE4S) {
        rect = CGRectMake(x, y, width, height);
    } else if (IPHONE5S) {
        rect = CGRectMake(x, y * (568 / height4s), width, height * (568 / height4s));
    } else if (IPHONE6) {
        rect = CGRectMake(x * (375 / width4s), y * (667 / height4s), width * (375 / width4s), height * (667 / height4s));
    } else {
        rect = CGRectMake(x * (414 / width4s), y * (736 / height4s), width * (414 / width4s), height * (736 / height4s));
    }
    return rect;
}

+ (CGRect)getBtnRectWithX:(CGFloat)x Y:(CGFloat)y width:(CGFloat)width height:(CGFloat)height
{
    CGRect rect = CGRectZero;
//    CGFloat height4s = 480.;
    CGFloat width4s = 320.;
    if (IPHONE4S) {
        rect = CGRectMake(x, y, width, height);
    } else if (IPHONE5S) {
        rect = CGRectMake(x, y, width, height);
    } else if (IPHONE6) {
        rect = CGRectMake(x * (375 / width4s), y, width * (375 / width4s), width * (375 / width4s));
    } else {
        rect = CGRectMake(x * (414 / width4s), y, width * (414 / width4s), width * (414 / width4s));
    }
    return rect;

}


@end
