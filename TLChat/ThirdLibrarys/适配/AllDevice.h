//
//  AllDevice.h
//  PlayInBeijing
//
//  Created by MS on 15-9-23.
//  Copyright (c) 2015年 MS. All rights reserved.
//

#import <Foundation/Foundation.h>

#define IPHONE4S (([[UIScreen mainScreen] bounds].size.width == 320) && ([[UIScreen mainScreen] bounds].size.height == 480))

#define IPHONE5S (([[UIScreen mainScreen] bounds].size.width == 320) && ([[UIScreen mainScreen] bounds].size.height == 568))

#define IPHONE6 (([[UIScreen mainScreen] bounds].size.width == 375) && ([[UIScreen mainScreen] bounds].size.height == 667))

#define IPHONE6P (([[UIScreen mainScreen] bounds].size.width == 414) && ([[UIScreen mainScreen] bounds].size.height == 736))
