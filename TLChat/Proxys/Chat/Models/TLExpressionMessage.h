//
//  TLExpressionMessage.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLMessage.h"
#import "TLEmoji.h"

@interface TLExpressionMessage : TLMessage

@property (nonatomic, strong) TLEmoji *emoji;

@property (nonatomic, strong, readonly) NSString *path;

@property (nonatomic, strong, readonly) NSString *url;

@property (nonatomic, assign, readonly) CGSize emojiSize;

@end
