//
//  TLMessageFrame.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TLMessageFrame : NSObject

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) CGSize contentSize;

@end
