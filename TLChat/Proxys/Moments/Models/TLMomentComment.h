//
//  TLMomentComment.h
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//

#import "TLBaseDataModel.h"

@interface TLMomentCommentFrame : NSObject

@property (nonatomic, assign) CGFloat height;

@end


@interface TLMomentComment : TLBaseDataModel

@property (nonatomic, strong) TLUser *user;

@property (nonatomic, strong) TLUser *toUser;

@property (nonatomic, strong) NSString *content;

@property (nonatomic, strong) TLMomentCommentFrame *commentFrame;

@end
