//
//  TLMomentComment.m
//  LES,GO
//
//  Created by liuchao on 16/6/14.
//  Copyright © 2016年 Zeersi. All rights reserved.
//
#import "TLMomentComment.h"

@implementation TLMomentComment

- (id)init
{
    if (self = [super init]) {
        [TLMomentComment mj_setupObjectClassInArray:^NSDictionary *{
            return @{ @"user" : @"TLUser",
                      @"toUser" : @"TLUser"};
        }];
    }
    return self;
}

#pragma mark - # Getter
- (TLMomentCommentFrame *)commentFrame
{
    if (_commentFrame == nil) {
        _commentFrame = [[TLMomentCommentFrame alloc] init];
        _commentFrame.height = 35.0f;
    }
    return _commentFrame;
}

@end



@implementation TLMomentCommentFrame

@end
